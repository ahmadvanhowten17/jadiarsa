@extends('frontend.layouts.auth')

@section('content')
    <div class="contaniner">
        <div class="card">
            <div class="row">
                <div class="col-md-6 log-picture position-relative">
                    <img src="{{ url('img/gedung.png') }}" class="img-fluid h-100" alt="" srcset="">
                    <div class="log-picture-abs">
                        <a href="{{ url('/') }}">
                            <img src="{{ url('logo.jpeg') }}" alt="" srcset="">
                            <span class="float-right">Jadiarsi</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 sec-login">
                    <div class="card border-0 card-regis">
                        <div class="card-body mx-6 bg-success text-center">
                            <h2 class="my-1 text-bold">Daftar</h2>
                            <span class="log-title">Sudah Memiliki Akun</span><br>
                            <a href="{{ url('login') }}">Masuk</a>
                            <form action="{{ route('register') }}" method="POST" class=" justify-content-center">
                                @csrf
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-user icon"></i>
                                        </span>
                                    </div>
                                    <input name="username" type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" id="inlineFormInputGroup" placeholder="username...">
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-envelope icon"></i>
                                        </span>
                                    </div>
                                    <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" id="inlineFormInputGroup" placeholder="Email...">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-lock icon"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" id="inlineFormInputGroup" placeholder="password...">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-key icon"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" id="inlineFormInputGroup" placeholder="confir password...">
                                    @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary btn-block mt-3">Daftar</button>
                                <div class="log-social">
                                    <p class="text-center text-white-50">Masuk Menggunakan</p>
                                    <a href="{{ url('register/facebook') }}" class="btn bg-blue fb"><i class="fab fa-facebook-f"></i></a>
                                    <a href="{{ url('register/google') }}" class="btn bg-danger fb"><i class="fab fa-google"></i></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
