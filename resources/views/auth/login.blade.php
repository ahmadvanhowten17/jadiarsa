@extends('frontend.layouts.auth')

@section('content')
    <div class="contaniner">
        <div class="card">
            <div class="row">
                <div class="col-md-6 log-picture position-relative">
                    <img src="{{ url('img/gedung.png') }}" class="img-fluid h-100" alt="" srcset="">
                    <div class="log-picture-abs">
                        <a href="{{ url('/') }}">
                            <img src="{{ url('logo.jpeg') }}" alt="" srcset="">
                            <span class="float-right">Jadiarsi</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 sec-login">
                    <div class="card border-0 card-login">
                        <div class="card-body bg-success mx-6 text-center">
                            <h2 class="my-1 text-bold">Masuk</h2>
                            <span class="log-title">Belum Punya Akun Jadiarsi ?</span><br>
                            <a href="{{ url('register') }}">Daftar</a>
                            <form action="{{ route('login') }}" method="POST" class=" justify-content-center">
                                @csrf
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-user icon"></i>
                                        </span>
                                    </div>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" id="inlineFormInputGroup" placeholder="Email...">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fas fa-key icon"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" class="form-control" id="inlineFormInputGroup" placeholder="password...">
                                </div>
                                <button class="btn btn-primary btn-block">Masuk</button>
                                <div class="log-social">
                                    <p class="text-center text-white-50">Masuk Menggunakan</p>
                                    <a href="{{ url('login/facebook') }}" class="btn bg-blue fb"><i class="fab fa-facebook-f"></i></a>
                                    <a href="{{ url('login/google') }}" class="btn bg-danger fb"><i class="fab fa-google"></i></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
