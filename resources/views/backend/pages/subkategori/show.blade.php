<div class="modal-body">
    <table class="table table-borderless">
        <tr>
            <th>Nama subkategori</th>
            <td>{{ $record->nama }}</td>
        </tr>
        <tr>
            <th>Nama Kategori</th>
            <td><span class="badge badge-danger">{{ $record->kategories->name }}</span></td>
        </tr>
        <tr>
            <th>Created By</th>
            <td>{{ $record->users->username }}</td>
        </tr>
    </table>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
  </div>
