<div class="modal-body">
    <form id="dataFormModal" action="{{ url("admin/user/$record->id") }}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" name="username" class="form-control" value="{{ $record->username ? $record->username : old('username') }}" placeholder="username">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="email" name="email" class="form-control" value="{{ $record->email ? $record->email : old('email') }}" placeholder="email">
        </div>
        <div class="form-group">
            <label for="">status</label>
            <select name="status" id="" class="form-control">
                <option value="superadmin" {{ $record->status === 'superadmin' ? 'selected' : ''  }}>superadmin</option>
                <option value="admin" {{ $record->status === 'admin' ? 'selected' : ''  }}>Admin</option>
                <option value="users" {{ $record->status === 'users' ? 'selected' : ''  }}>Users</option>
            </select>
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan" data-url="{{ url($pageUrl) }}">Save changes</button>
  </div>
