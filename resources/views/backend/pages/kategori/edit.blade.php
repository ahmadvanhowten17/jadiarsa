<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl/$record->id") }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $record->name ? $record->name : old('name') }}" placeholder="nama kategori">
            @error('name')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
