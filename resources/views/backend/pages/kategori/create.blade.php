<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl") }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('nama') }}" placeholder="nama kategori">
            @error('name')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
