<div class="modal-body">
    <table class="table table-borderless">
        <tr>
            <th>Nama Kategori</th>
            <td>{{ $record->name }}</td>
        </tr>
    </table>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
</div>
