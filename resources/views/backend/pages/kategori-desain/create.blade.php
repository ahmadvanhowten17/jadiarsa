<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl") }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}" placeholder="nama kategori">
            @error('nama')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Upload Photo</label>
            <input name="atachment[]" type="file" class="form-control" multiple>
            <span class="text-danger">Upload image lebih dari satu data</span>
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
