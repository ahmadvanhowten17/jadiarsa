<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl") }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Kateori</label>
            <select name="sub_kategorie_id" id="" class="form-control">
                <option value="">Pilih Sub Kategori</option>
                @foreach ($record as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ old('nama') }}" placeholder="nama subkategori">
            @error('nama')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan" data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
