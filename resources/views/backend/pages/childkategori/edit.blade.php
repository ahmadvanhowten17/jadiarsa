<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl/$record->id") }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="form-group">
            <label for="">Sub kategori</label>
            <select name="sub_kategori_id" id="" class="form-control">
                @foreach ($kategori as $item)
                    <option value="{{ $item->id }}" {{ $item->id === $record->sub_kategorie_id ? 'selected' : '' }}>{{ $item->nama }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" value="{{ $record->nama ? $record->nama : old('nama') }}" placeholder="nama kategori">
            @error('nama')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
