<div class="modal-body">
    <table class="table table-borderless">
        <tr>
            <th>Nama Child Kategori</th>
            <td>{{ $record->nama }}</td>
        </tr>
        <tr>
            <th>Nama Sub Kaegori</th>
            <td><span class="badge badge-danger">{{ $record->subkategories->nama }}</span></td>
        </tr>
        <tr>
            <th>Created By</th>
            <td>{{ $record->user->username }}</td>
        </tr>
    </table>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
  </div>
