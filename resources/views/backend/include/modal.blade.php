<script>
    function loadmodal(url, id='', title=''){
        $.ajax({
            type: "GET",
            url: url,
            data:{
                id:id,
                title:title
            },
            success: function(resp){
                $('.modal').modal('show');
                $('.modal-title').html(title);
                $('.form-modal').html(resp);
            },
            error: function(resp){
            $('.body-isi').html('data tidak d temukan');
            }
        });
    }
    function showBoxValidation(resp){
        var temp = ``;
        if(resp.statusText = 'Unprocessable Entity'){
            temp += `<div class="sidebar-widget wow fadeInUp outer-top-vs animated" style="visibility: visible; animation-name: fadeInUp;"><h3 class="section-title text-warning">Informasi</h3><div class="sidebar-widget-body" ><div class="compare-report"><ul class="list text-left bold" style="font-size:16px;list-style:inside;">`;
                if(resp.responseJSON){
                    if(resp.responseJSON.errors){
                        var data = resp.responseJSON.errors;
                        $.each(data,function(key,value){
                            temp += `<li><small>`+upperCase(key.replace("_", " "))+` : ` +value[0]+ `</small></li>`;
                        });
                    }
                }
            temp += `</ul></div></div></div>`;
        }else{
            temp = 'Terjadi Kesalahan Sistem';
        }
        console.log('temp',temp);
        return temp;
    }
    function showFormErrorModalTwo(resp, formid){
    	var response = resp.responseJSON;
        var addErr = {};

        $.each(response.errors, function (index, val) {
            var response = resp.responseJSON;
            if (index.includes(".")) {
                res = index.split('.');
                index = '';
                for (i = 0; i < res.length; i++) {
                    if (i == 0) {
                        res[i] = res[i];
                    } else {
                        if (res[i] == 0) {
                            res[i] = '[0]';
                        } else {
                            res[i] = '[' + res[i] + ']';
                        }
                    }
                    index += res[i];
                }
            }
            clearFormErrorModalTwo(index,val,formid);
            var name = index.split('.').reduce((all, item) => {
                all += (index == 0 ? item : '[' + item + ']');
                return all;
            });

            console.log('index',index)
            console.log('name',name)
            var fg = $('[name="' + name + '"], [name="' + name + '[]"]');
            fg.addClass('has-error');

            fg.after('<small class="control-label error-label font-bold" style="margin-top: 0.25rem;font-size: smaller;color: #ea5455;">' + val + '</small>')
        });
        $("html, body").animate({ scrollTop: 0 }, "slow");
        var intrv = setInterval(function(){
            $('.error-label').slideUp(500, function(e) {
                $(this).remove();
                $('.has-error').removeClass('has-error');
                clearTimeout(intrv);
            });
        }, 14000)
    }
    function saveFormModal(form)
    {
        $(form).ajaxSubmit({
            success: function(resp){
                $(".modal").modal('hide');
                swal(
                    'Tersimpan!',
                    'Data Berhasil Di Simpan.',
                    'success'
                    ).then((result) => {
                        var url = "{{ url($pageUrl) }}";
                        window.location = url;
                        if(resp.url){
                        }
                        dt.draw();
                    })
                },
                error: function(resp){
                    swal(
                    'Gagal Menyimpan Data!',
                    showBoxValidation(resp),
                    'error'
                    );
                    showFormErrorModalTwo(resp,form);
                }
        });
    }
    function deletform(urls){
        swal({
            title: 'Apa Anda Yakin?',
            text: "Data Yang Di Hapus Tidak Dapat Di Kembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
          }).then((result) => {
            if (result) {
              $.ajax({
                type: 'POST',
                url: urls,
                data: {_token: "{{ csrf_token() }}", _method: "delete"},
                success: function(resp){
                  swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  ).then(function(e){
                    if(resp){
                      var url = "{{ url($pageUrl) }}";
                      window.location = url;
                    }
                  });
                },
                error: function(resp){
                  swal(
                    'Gagal!!',
                    'Data gagal dihapus, karena sedang dipakai',
                    'error'
                  ).then(function(e){
                    if(resp){
                      var url = "{{ url($pageUrl) }}";
                      window.location = url;
                    }
                  });
                }
              });
            }
          });
    }
</script>










<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="form-modal">

      </div>
    </div>
  </div>
</div>
