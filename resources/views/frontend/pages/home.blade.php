@extends('frontend.layouts.default')
@section('header')
    <header>
        <div class="carousel-wrapper">
            <div class="loop owl-carousel owl-theme">
                <div class="item">
                    <img src="{{ url('img/shop1.png') }}" alt="">
                </div>
                <div class="item">
                    <img src="{{ url('img/shop2.png') }}" alt="">
                </div>
                <div class="item">
                    <img src="{{ url('img/shop3.png') }}" alt="">
                </div>
            </div>
        </div>
    </header>
@endsection
@section('content')
<section class="section-service">
    <div class="container">
        <div class="card">
            <div class="card-title text-center mt-3 card-judul">
                <h2 class="">Koleksi Spesial Untuk Anda</h2>
            </div>
            <hr>
            <div class="loop-product owl-carousel owl-theme">
                @if ($record->count() > 0)
                    @foreach ($record as $item)
                        <div class="item p-2">
                            <img src="{{'storage/'.$item->attachment()->orderBy('created_at','asc')->first()->url }}" alt="" class="card-img-top" height="170px">
                            <div class="card-body" style="height: 100px !important">
                                <a href="{{ url("p/$item->slug") }}" class="" style="list-style: none; color: #000000; font-size: 20px; font-weight: bold !important"><span>{{ $item->nama_barang }}</span></a>
                                <p class="card-text">Rp. {{ number_format($item->harga_barang,'2',',','.') }}</p>
                            </div>
                        </div>
                    @endforeach
                @else
                <center>Produk Belum Tersedia</center>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
