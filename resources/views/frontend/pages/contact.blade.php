@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">contact</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                <div class="col-md-6 position-relative pr-0">
                    <img src="{{ url('img/gedung.png') }}" class="img-fluid h-100" alt="" srcset="">
                    <div class="about-abs">
                        <div class="d-flex">
                            <div class="flex-coloumn">
                                @for ($i = 1; $i <= 3; $i++)
                                    <img src="{{ url("img/about$i.png") }}" class="img-fluid" alt="">
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h1 class="card-title">CONTACT</h1><hr><hr>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <tr>
                                <th>Email <span class="float-right">:</span></th>
                                <td> jadiarsi@gmail.com</td>
                            </tr>
                            <tr>
                                <th>No. Telp <span class="float-right">:</span></th>
                                <td> 08123456789</td>
                            </tr>
                            <tr>
                                <th>Alamat Kantor <span class="float-right">:</span></th>
                                <td> Bandung</td>
                            </tr>
                        </table>
                    </div>
                    <h1 class="card-title mt-5">Kritik & Saran </h1><hr><hr>
                    <div class="card-body">
                        <p>
                            Jadiarsi.com butuh masukan dari kalian nih, jangan ragu semua kritik & saran kalian sangat bermanfaat untuk kami
                        </p>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="">Kritik & saran</label>
                                <input type="text" class="form-control" placeholder="Masukan Kritik & saran....">
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" placeholder="Masukan Nama....">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat Email & saran</label>
                                <input type="email" class="form-control" placeholder="Masukan Email...">
                            </div>
                            <button class="btn btn-success btn-block">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection























