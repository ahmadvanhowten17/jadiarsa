@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">Shop</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                @for ($i = 1; $i <= 3; $i++)
                    <div class="col-md-4 position-relative">
                        <div class="card-body">
                            <img src="{{ url("img/shop$i.png") }}" class="img-fluid" alt="" srcset="">
                        </div>
                        <span class="shop-abs"></span>
                        @if ($i === 1)
                            <div class="shop-title">
                                <p class="text-success">Furnuter Impian</p>

                            </div>
                        @elseif($i === 2)
                            <div class="shop-title">
                                <p class="text-primary">aksesoris Rumah kekinian</p>

                            </div>
                        @elseif($i === 3)
                            <div class="shop-title">
                                <p class="text-secondary">alat-alat Buat kamu yang lagi proyek</p>
                            </div>
                        @endif
                    </div>
                @endfor
                <div class="shop-card">
                    <h4 class="text-center">KAMU LAGI NYARI APA ? Sini <br> <a href="{{ url('/') }}" class="text-decoration-none">jadiarsi.com</a> bantu <br> Ada rekomendasi buat kamu</h4>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-work-desain" style="margin-top: -150px">
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h2 class="text-center mt-5">Furniture</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body position-relative">
                        <img src="img/shop1.png" class="img-fluid" alt="" srcset="">
                        <span class="span-img"></span>
                        <div class="title-design">
                            <div class="d-flex">
                                <div class="flec-coloumn furnuter">
                                    <h1>Furniture</h1>
                                    <p>- Rekomendasi</p>
                                    <p>- Tempat tidur</p>
                                    <p>- Lemari</p>
                                    <p>- Sofa</p>
                                    <p>- Rak Penyimpanan</p>
                                    <p>- Meja</p>
                                    <p>- Set Ruangan</p>
                                    <p class="mt-2 mb-3">Aksesoris & Perlengkapan Rumah</p>
                                    <p>Alat-Alat Konstruksi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        @for ($i = 1; $i <=  6; $i++)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{ url("img/furniture$i.png") }}" alt="" srcset="" class="img-fluid" height="100%">
                                    </div>
                                    <div class="card-foot">
                                        @if ($i === 1)
                                        <p class="text-center">tempat tidur</p>
                                        @elseif($i === 2)
                                        <p class="text-center">Lemari</p>
                                        @elseif($i === 3)
                                        <p class="text-center">Sofa</p>
                                        @elseif($i === 4)
                                        <p class="text-center">Kursi</p>
                                        @elseif($i === 5)
                                        <p class="text-center">Rak Penyimpanan</p>
                                        @elseif($i === 6)
                                        <p class="text-center">Meja</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endfor
                        <div class="text-center">
                            <div class="budgeting">
                                <p>bingung cari furniture yang cocok ?<br>
                                    Sini cek rekomendasinya jadiarsi.com </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-work-desain mt-5">
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h2 class="text-center mt-5">Home equipment & accesories</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body position-relative">
                        <img src="img/shop2.png" class="img-fluid" alt="" srcset="">
                        <span class="span-img"></span>
                        <div class="title-design">
                            <div class="d-flex">
                                <div class="flec-coloumn furnuter">
                                    <h4>Home equipment & accesories</h4>
                                    <p>- Rekomendasi</p>
                                    <p>- Perlengkapan Dapur</p>
                                    <p>- Perlengkapan Kamar Mandi</p>
                                    <p>- Perlengkapan Kamar Tidur</p>
                                    <p>- Perlengkapan Ruang Keluarga</p>
                                    <p>- Electrical Equipment</p>
                                    <p>- Smart Home</p>
                                    <p class="mt-2 mb-3">Furniture</p>
                                    <p>Alat-Alat Konstruksi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row position-relative">
                        @for ($i = 1; $i <=  6; $i++)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <img src="{{ url("img/furniture$i.png") }}" alt="" srcset="" class="img-fluid" height="100%">
                                    </div>
                                    <div class="card-foot">
                                        @if ($i === 1)
                                        <p class="text-center">Perlengkapan <br> Dapur</p>
                                        @elseif($i === 2)
                                        <p class="text-center">Perlengkapan Kamar Mandi</p>
                                        @elseif($i === 3)
                                        <p class="text-center">Perlengkapan Kamar Tidur</p>
                                        @elseif($i === 4)
                                        <p class="text-center">Perlengkapan Ruang Keluarga</p>
                                        @elseif($i === 5)
                                        <p class="text-center">Electical Equipment</p>
                                        @elseif($i === 6)
                                        <p class="text-center">Smart Home</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endfor
                        <div class="text-center">
                            <div class="budgeting">
                                <p>kurang update perlengkapan rumah kamu, tapi bingung mana yang bagus ? <br> Sini cek rekomendasinya <a href="{{ url('/') }}">jadiarsi.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-work-desain mt-5">
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h2 class="text-center mt-5">Alat-Alat konstruksi</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body position-relative">
                        <img src="img/shop2.png" class="img-fluid" alt="" srcset="">
                        <span class="span-img"></span>
                        <div class="title-design">
                            <div class="d-flex">
                                <div class="flec-coloumn furnuter">
                                    <h4>Alat-Alat konstruksi</h4>
                                    <p>- Rekomendasi</p>
                                    <p>- Kebutuhan Konstruksi Bangunan Basah</p>
                                    <p>- Kebutuhan Konstruksi Jalan</p>
                                    <p>- Kebutuhan Konstruksi Jembatan</p>
                                    <p>- Kebutuhan Konstruksi Banunan Kering</p>
                                    <p>- Sewa Alat Konstruksi Ringan</p>
                                    <p>- Sewa Alat Konstruksi Berat</p>
                                    <p class="mt-2 mb-3">Furniture</p>
                                    <p>Home Equipment & Accesories</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row position-relative">
                        @for ($i = 1; $i <=  6; $i++)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <img src="{{ url("img/furniture$i.png") }}" alt="" srcset="" class="img-fluid" height="100%">
                                    </div>
                                    <div class="card-foot">
                                        @if ($i === 1)
                                        <p class="text-center">Kebutuhan Konstruksi Bangunan Basah</p>
                                        @elseif($i === 2)
                                        <p class="text-center">Kebutuhan Konstruksi Jalan</p>
                                        @elseif($i === 3)
                                        <p class="text-center">Kebutuhan Konstruksi Jembatan</p>
                                        @elseif($i === 4)
                                        <p class="text-center">Kebutuhan Konstruksi Banunan Kering</p>
                                        @elseif($i === 5)
                                        <p class="text-center">Sewa Alat Konstruksi Ringan</p>
                                        @elseif($i === 6)
                                        <p class="text-center">Sewa Alat Konstruksi Berat</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endfor
                        <div class="text-center">
                            <div class="budgeting">
                                <p>bingung cari furniture yang cocok ?
                                    Sini cek rekomendasinya jadiarsi.com
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
