<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl") }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama Toko</label>
            <input type="text" name="nama_toko" class="form-control @error('nama_toko') is-invalid @enderror" value="{{ old('nama_toko') }}" placeholder="Nama Toko">
            @error('nama_toko')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Deskripsi Toko</label>
            <textarea name="deskripsi_toko" id="" class="form-control @error('deskripsi_toko') is-invalid @enderror" cols="30" rows="2" placeholder="Deskripsi Toko"></textarea>
            @error('deskripsi_toko')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">No Telp</label>
            <input type="number" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ old('phone') }}" placeholder="Number Telphone">
            @error('phone')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Kode Pos</label>
            <input type="number" name="kode_pos" class="form-control @error('kode_pos') is-invalid @enderror" value="{{ old('kode_pos') }}" placeholder="Kode Pos">
            @error('kode_pos')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <center><h3>Pilih Alamat Anda</h3></center><hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Provinsi</label>
                        <select name="provincie_id" class="select2 form-control data-provin" data-url="{{ url('myprofile') }}">
                            <option value="">Pilih Provinsi</option>
                            @foreach ($provinsi as $prov)
                                <option value="{{ $prov->id }}">{{ $prov->provinsi }}</option>
                            @endforeach
                        </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Pilih Kec/Kab</label>
                    <select name="citie_id" id="list-kota" class="form-control select2">
                        <option value="">Pilih Kec/Kab</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" name="alamat_toko" class="form-control @error('alamat_toko') is-invalid @enderror" value="{{ old('alamat_toko') }}" placeholder="Alamat Toko">
            @error('alamat_toko')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
