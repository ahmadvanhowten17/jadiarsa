<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl/$record->id") }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $record->id }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nama Toko</label>
                    <input type="text" name="nama_toko" value="{{ $record->nama_toko }}" class="form-control @error('nama_toko') is-invalid @enderror" value="{{ old('nama_toko') }}" placeholder="Nama Toko">
                    @error('nama_toko')
                        <div class="message">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Type Toko</label>
                    <select name="type_toko" id="" class="select2 form-control">
                        <option value="Toko Furniture" {{ $record->type_toko === 'Toko Furniture' ? 'selected' : '' }}>Toko Furniture</option>
                        <option value="Toko Matrial" {{ $record->type_toko === 'Toko Matrial' ? 'selected' : '' }}>Toko Matrial</option>
                        <option value="Toko Alat Dan Perlengkapan" {{ $record->type_toko === 'Toko Alat Dan Perlengkapan' ? 'selected' : '' }}>Toko Alat Dan Perlengkapan</option>
                        <option value="Toko Home Appliances" {{ $record->type_toko === 'Toko Home Appliances' ? 'selected' : '' }}>Toko Home Appliances</option>
                    </select>
                    @error('type_toko')
                        <div class="message">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Deskripsi Toko</label>
            <textarea name="deskripsi_toko" id="" class="summernote form-control @error('deskripsi_toko') is-invalid @enderror" cols="30" rows="2" placeholder="Deskripsi Toko">{!! $record->deskripsi_toko !!}</textarea>
            @error('deskripsi_toko')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">No Telp</label>
            <input type="number" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ $record->phone }}" placeholder="Number Telphone">
            @error('phone')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Kode Pos</label>
            <input type="number" name="kode_pos" class="form-control @error('kode_pos') is-invalid @enderror" value="{{ $record->kode_pos }}" placeholder="Kode Pos">
            @error('kode_pos')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
        <center><h3>Pilih Alamat Anda</h3></center><hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Provinsi</label>
                    <select name="provin_id" id="provins" class="select2 form-control data-provin" data-url="{{ url('myprofile') }}">
                        <option value="">Pilih Provinsi</option>
                        @foreach ($provinsi as $item)
                            <option value="{{ $item->id }}" {{ $item->id === $record->provincie_id ? 'selected' : '' }}>{{ $item->provinsi }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Kabupaten / Kota</label>
                    <select name="city_id" id="list-kota" class="select2 form-control">
                        <option value="">Pilih Kabupaten / Kota</option>
                        @if ($record->citie_id)
                            @foreach ($kota->where('provin_id',$record->provincie_id)->get() as $item)
                                <option value="{{ $item->id }}" {{ $item->id === $record->citie_id ? 'selected' : '' }}>{{$item->type .' '. $item->kota }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" name="alamat_toko" class="form-control @error('alamat_toko') is-invalid @enderror" value="{{ $record->alamat_toko }}" placeholder="Alamat Toko">
            @error('alamat_toko')
                <div class="message">{{ $message }}</div>
            @enderror
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
  </div>
