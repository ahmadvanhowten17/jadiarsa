@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card" style="min-height: 30vh">
            <div class="card-title">
                <center><h2 class="text-bold">Jadi Punya Toko</h2></center>
            </div>
            @if ($record)
                <div class="card-body">
                    <center>
                        <a href="{{ url('jual-barang') }}" class="btn btn-primary">Jual Barang</a>
                        <a href="javascript:void(0)" class="btn btn-success btn-edit" data-url="{{ url($pageUrl.'/'.$record->id.'/edit') }}" data-title="Edit Toko">Edit Toko</a>
                    </center>
                </div>
            @else
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Verivikasi Email</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Buat Toko</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card-body">
                                <center>
                                    <a href="javascript:void(0)" class="btn btn-success btn-veriv {{ $verif || auth()->user()->status_veriv === 'verived' ?  'd-none' : '' }}" data-url="{{ url("$pageUrl") }}">Verivikasi Email Anda</a>
                                    <div class="input-group w-25 mt-3 {{ $verif ?  '' : 'd-none' }}">
                                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Masukan kode verivikasi">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-primary btn-kode" data-url="{{ url($pageUrl) }}" data-id="{{ Auth::id() }}">Kirim</button>
                                        </div>
                                    </div>
                                </center>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card-body">
                                <center><a href="javascript:void(0)" class="btn btn-success {{ auth()->user()->status_veriv === null ? 'disabled' : 'add-data' }}" data-url="{{ url("$pageUrl/create") }}" data-title="Jadi Punya Toko">Buat Toko</a></center>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>
<section class="section-deskripsi" style="margin-top: -150px">
    <div class="container">
        <div class="card">
            <div class="list-product"></div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="form-modal">

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            var url = "{{ url($pageUrl) }}/"+ 'show-product';
            listProduct(url);
        });
    </script>
@endsection




















