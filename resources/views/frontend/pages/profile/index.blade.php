@extends('frontend.layouts.default')
@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-title mt-3">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <h5 class="text-center">My Profile</h5><hr>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-body">
                        <div class="mx-5 text-center">
                            <img src="{{ ($record->pictures()->orderBy('created_at','desc')->first()) ? url('storage/'.$record->pictures()->orderBy('created_at','desc')->first()->url) : url('user.png') }}" class="rounded-circle" style="margin:30% 0px; width: 50%; height: 200px;" alt="" srcset="">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-body">
                        <form action="{{ url("myprofile/$record->id") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="id" value="{{ $record->id }}">
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" value="{{ $record->username ? $record->username : old('username') }}">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $record->email  }}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Jenis Kelamin</label>
                                <select name="gender" id="" class="form-control @error('gender') is-invalid @enderror">
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="Laki - Laki" {{ $record->gender === 'Laki - Laki' ? 'selected' : '' }}>Laki - Laki</option>
                                    <option value="Perempuan" {{ $record->gender === 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                                </select>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="card border-0">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Upload Photo</label>
                                        <input type="file" name="url" id="" class="form-control-file @error('url') is-invalid @enderror" accept="image/*">
                                        @error('url')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-title">
                                <h3 class="text-center text-bold">Pilih Alamat Anda</h3><hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Provinsi</label>
                                            <select name="provin_id" id="provins" class="form-control data-provin" data-url="{{ url('myprofile') }}">
                                                <option value="">Pilih Provinsi</option>
                                                @foreach ($provinsi as $item)
                                                    <option value="{{ $item->id }}" {{ $item->id === $record->provin_id ? 'selected' : '' }}>{{ $item->provinsi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Kabupaten / Kota</label>
                                            <select name="city_id" id="list-kota" class="form-control">
                                                <option value="">Pilih Kabupaten / Kota</option>
                                                @if ($record->city_id)
                                                    @foreach ($kota as $item)
                                                        <option value="{{ $item->id }}" {{ $item->id === $record->city_id ? 'selected' : '' }}>{{$item->type .' '. $item->kota }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Alamat</label>
                                    <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" value="{{ $record->alamat }}">
                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{-- <script>
        $(document).on('click','#provins',function(e){
            e.preventDefault();
            var id = $(this).val();
            var _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{ url('myprofile') }}",
                data: {
                    id:id,
                    _token:_token
                },
                success: function(resp){
                    $('#list-kota').html(resp);
                }
            });
        });
    </script> --}}
@endsection
