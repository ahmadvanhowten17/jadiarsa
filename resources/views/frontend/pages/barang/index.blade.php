@extends('frontend.layouts.default')
@section('fileInput')
<link href="{{ url('library/bootstrap-fileinput/css/fileinput.css') }}" media="all" rel="stylesheet" type="text/css"/>
<link href="{{ url('library/bootstrap-fileinput/themes/explorer-fas/theme.css') }}" media="all" rel="stylesheet" type="text/css"/>
@endsection
@section('script-fileInput')
<script src="{{ url('library/bootstrap-fileinput/js/plugins/piexif.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/js/plugins/sortable.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/js/fileinput.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/js/locales/fr.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/js/locales/es.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/themes/fas/theme.js') }}" type="text/javascript"></script>
<script src="{{ url('library/bootstrap-fileinput/themes/explorer-fas/theme.js') }}" type="text/javascript"></script>
@endsection
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Jadiarsi</a></li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card" style="min-height: 30vh">
            <div class="card-title mt-3">
                <center><h2 class="text-bold">Input Barang</h2></center>
            </div>
            <div class="card-body">
                <form id="dataFormModal" action="{{ url($pageUrl) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="lapak_id" value="{{ $lapak->id }}">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="kategori_id" id="" class="form-control data-kat" data-url="{{ url('kategori') }}">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($kategori as $kat)
                                        <option value="{{ $kat->id }}">{{ $kat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="sub_kategorie_id" id="" class="form-control data-sub" data-url="{{ url('child-kategori') }}">
                                    <option value="">Pilih Subkategori</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="child_kategorie_id" id="" class="form-control data-child">
                                    <option value="">Pilih ChildKategori</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nama Barang</label>
                                <input name="nama_barang" type="text" class="form-control" placeholder="Nama barang">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Stock Barang</label>
                                <input name="stock_barang" type="text" class="form-control" placeholder="Stock barang">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Berat Barang (gram)</label>
                                <input name="berat_barang" type="text" class="form-control" placeholder="Berat barang">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Kondisi Barang</label>
                                <select name="kondisi_barang" id="" class="form-control">
                                    <option value="baru">Baru</option>
                                    <option value="bekas">Bekas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Deskripsi Barang</label>
                        <textarea name="deskripsi_barang" id="summernote" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Harga Normal</label>
                                <input name="harga_normal" type="text" class="form-control change-money">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Disc Barang</label>
                                <input name="disc_barang" type="text" class="form-control disc" placeholder="masukan Tanpa %">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Sub Total</label>
                        <input name="harga_barang" type="text" class="form-control sub-total" readonly>
                    </div>
                    <div class="file-loading">
                        <input id="input-700" name="atachment[]" type="file" data-show-upload="false" multiple>
                    </div>
                    {{--  <div class="form-group">
                        <label for="">Upload Photo Barang</label>
                        <input name="atachment[]" type="file" class="form-control" multiple>
                    </div>  --}}
                    <a href="javascript:void(0)" class="btn btn-success btn-block btn-simpan mt-2" data-url="{{ url('jadi-punya-toko') }}">Simpan</a>
                    {{-- <button type="submit" class="btn btn-success btn-block">Simpan</button> --}}
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('#summernote').summernote({
                placeholder: 'Silahkan Masukan Deskripsi Barang..',
                tabsize: 2,
                height: 120,
                toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ]
            });
            numerik();
            fileInput();
        });
    </script>
@endsection



















