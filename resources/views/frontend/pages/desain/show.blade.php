@extends('frontend.layouts.default')
@section('style')
    <style>
        .whatsup{
            display: none !important;
        }
    </style>
@endsection
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                {{-- <div class="col-md-4">
                    <p class="card-title">Sortir Berdasarkan </p>
                    <ul>
                        <li></li>
                    </ul>
                </div> --}}
                <div class="col-md-12">
                    <div class="card-title p-2">
                        <select name="" id="" class="form-control" style="width: 180px">
                            <option value="">Pilih Type Desain</option>
                            <option value="A">Type A</option>
                            <option value="B">Type B</option>
                            <option value="C">Type C</option>
                        </select>
                    </div>
                    <div class="row">
                        @foreach ($record as $desain)
                            <div class="col-md-3 mx-2 col-sm-6">
                                <div class="card-title mb-0">
                                    <img src="{{ ($desain->lapak->user->pictures()->orderBy('created_at','desc')->first()) ? url('storage/'.$desain->lapak->user->pictures()->orderBy('created_at','desc')->first()->url) : url('user.png') }}" alt="" class="rounded-circle" height="20" width="20">
                                    <span>{{ $desain->lapak->user->username }}</span>
                                </div>
                                <a href="{{ url("portfolio/$desain->slug") }}" class="text-decoration-none">
                                    <img src="{{ url('storage/'.$desain->attachment()->first()->url) }}" alt="" class="card-img-top img-fluid">
                                    <div class="card-body p-0 text-black-50">
                                        <h4 class="card-text float-left mr-2">Rp {{ number_format($desain->harga_interior,'2',',','.') }}</h4>
                                        <h4 class="card-text">{{ $desain->jumlah_lantai }} lantai</h4>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
{{-- @section('script')
    <script>
        $(document).ready(function(){
            var url = "{{ url($pageUrl) }}/"+'list/barang-terkait';
            var id = $('.product').data('id');
            var idBarang = "{{ $record->id }}";
            listProduct(url,id,idBarang);
        });
    </script>
@endsection --}}
