@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Jadiarsi</a></li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card" style="min-height: 20vh">
            <h2 class="card-title text-center">Request Ke Arsitek</h2>
            <form class="mx-1">
                <div class="form-row align-items-center">
                  <div class="col-4">
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Name">
                  </div>
                  <div class="col-4">
                    <label class="sr-only" for="inlineFormInput">Name</label>
                    <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Email">
                  </div>
                  <div class="col-3">
                    <label class="sr-only" for="inlineFormInputGroup">Username</label>
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">62</div>
                      </div>
                      <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="8xxxxxxx">
                    </div>
                  </div>
                  <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                  </div>
                </div>
            </form>
        </div>
    </div>
</section>
<section class="section-deskripsi" style="margin-top: -180px">
    <div class="container">
        <div class="card">
            <div class="row">
                @foreach ($record as $item)
                    <div class="col-md-4">
                        <a href="{{ url('desain/d/'.$item->slug) }}" class="text-decoration-none text-black">
                            @if ($item->attachment->count() > 0)
                                <img src="{{ url('storage/'.$item->attachment()->first()->url)  }}" alt="" class="card-img-top" height="300px">
                            @endif
                            <div class="card-body text-white" style="background-color: #000000">
                                <h5 class="card-title">{{ $item->nama }}</h5>
                                <p class="card-text">Contoh Desain {{ $item->nama }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="form-modal">

      </div>
    </div>
  </div>
</div>
@endsection




















