@extends('frontend.layouts.default')
@section('style')
    <style>
        .whatsup{
            display: none !important;
        }
    </style>
@endsection
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <header class="p-0">
            <div class="carousel-wrapper">
                <div class="loop owl-carousel owl-theme">
                    @foreach ($record->attachment as $image)
                        <a href="javascript:void(0)" class="item">
                            <img src="{{ url('storage/'.$image->url) }}" alt="" class="show-img" height="500">
                        </a>
                    @endforeach
                </div>
            </div>
        </header>
    </div>
    <div class="container">
        <div class="card">
            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="card-title">
                            <h1>{{ $record->nama_interior }}</h1>
                            <span class="card-text text-black-50 pr-3"><i class="fas fa-map-marker-alt"></i> {{ $record->city->kota }}</span>
                            <span class="card-text text-black-50 "><i class="fas fa-th"></i> {{ $record->kategoridesain->nama }}</span>
                        </div>
                        <h5>Deskripsi </h5>
                        <p>
                            {!! $record->deskripsi_interior !!}
                        </p>
                        <h5>Timeline Pengerjaan</h5>
                        <p class="float-left pr-2">{{ str_replace('-',' ',$record->estimasi) }}</p>
                        <p>({{ $pengerjaan .' Bulan' }})</p>
                        <hr>
                        <div class="card-title">
                            <h1>Lihat isi desain</h1>
                        </div>

                        @foreach ($record->attachment as $image)
                            <img src="{{ url('storage/'.$image->url) }}" alt="" class="show-img" height="100" height="100">
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-body">
                        <div class="card-title">
                            <p class="float-left">Didesain oleh {{ $record->lapak->user->username }}</p>
                            <img src="{{ ($record->lapak->user->pictures()->orderBy('created_at','desc')->first()) ? url('storage/'.$record->lapak->user->pictures()->orderBy('created_at','desc')->first()->url) : url('user.png') }}" alt="" class="rounded-circle float-right" height="40" width="40">
                            <div class="clearfix mb-2"></div>
                            <div class="service-desain border p-2">
                                <p class="text-center">Tertarik Dengan Desain Ini</p>
                                <button class="btn btn-success btn-block">Tanya Harga</button>
                                <p class="text-center text-black-50">Anda belum akan terkena biaya</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--  <section class="section-deskripsi mt-3">
    <div class="container">
        <div class="card">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Deskripsi Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Informasi Barang</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="ml-3 mt-3">
                        {!! $record->deskripsi_barang !!}
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table table-borderless">
                        <tr>
                            <th width="150px">Stok Barang <span class="float-right">:</span></th>
                            <td>{{ $record->stock_barang ?? '0'}}</td>
                        </tr>
                        <tr>
                            <th width="150px">Berat Barang <span class="float-right">:</span></th>
                            <td>{{ $record->berat_barang ?? '0'}} (gr)</td>
                        </tr>
                        <tr>
                            <th width="150px">Terjual <span class="float-right">:</span></th>
                            <td>{{ $record->barang_terjual ?? '0'}}</td>
                        </tr>
                        <tr>
                            <th width="150px">Nama Toko <span class="float-right">:</span></th>
                            <td>{{ $record->lapaks->nama_toko }}</td>
                        </tr>
                        <tr>
                            <th width="150px">Alamat Toko <span class="float-right">:</span></th>
                            <td>{{ $record->lapaks->provincies->provinsi.' '.$record->lapaks->cities->kota ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-work-desain mt-3">
    <div class="container list-product">

    </div>
</section>  --}}
<section class="section-cart">
    <a href="javascript:void(0)" class="btn-cart">
        <i class="fas fa-cart-plus fa-lg mr-2"></i>
        Add to Cart
    </a>
    <a href="https://web.whatsapp.com/send?phone=6282218422670&text=Halo%20Jadiarsi%2C%20saya%20ingin%20bertanya%20terkait%20produk%20{{ url($pageUrl.'/') }}" class="btn-pesan">
        <i class="fab fa-whatsapp fa-lg mr-2"></i>
        Chat
    </a>
</section>
@endsection
@section('script')
@endsection
