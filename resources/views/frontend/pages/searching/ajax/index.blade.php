<div class="card-body">
    <div class="row">
        @foreach ($record as $item)
            <div class="col-md-4">
                <a href="{{ url('p/'.$item->slug) }}" class=" text-decoration-none" style="font-size: 24px; color: #000000; font-weight: blod">
                    <img src="{{ $item->attachment->first() ? url('storage/'.$item->attachment->first()->url) : url('user.png') }}" alt="" class="card-img-top" height="150">
                    <h5 class="card-title">{{ $item->nama_barang }}</h5>
                    <span class="">Rp. {{ number_format($item->harga_barang,'2',',','.') }}</span>
                </a>
            </div>
        @endforeach
    </div>
</div>
<div class="card">
    <div class="float-right">
        {!! $record->links() !!}
    </div>
</div>