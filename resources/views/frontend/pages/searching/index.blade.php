@extends('frontend.layouts.default')
@section('content')
    <div class="container mt-3">
        <div class="row" style="min-height: 80vh">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        {{-- {{ dd($request->slug) }} --}}
                        <p class="card-title" style="font-size: 18px; font-weight: bold;">Filter Lokasi Terdekat</p>
                        <div class="form-group">
                            <label for="">Wilayah Provinsi</label>
                            <select name="ampas_provinsi" id="ampas_provinsi" class="select2 form-control data-provin" data-url="{{ url('myprofile') }}">
                                <option value="">Pilih Lokasi</option>
                                @foreach ($provinsi as $prov)
                                    <option value="{{ $prov->id }}">{{ $prov->provinsi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Wilayah Kota / Kab</label>
                            <select name="ampas_kota" id="list-kota" class="form-control select2">
                                <option value="">Pilih Kec/Kab</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-body">
                        <p class="card-title" style="font-size: 18px; font-weight: bold;">Filter Harga</p>
                        <div class="input-group">
                            <input type="number" class="form-control" name="ampas_harga" value="" id="inlineFormInputGroup" placeholder="Harga...">
                            {{--  <div class="input-group-prepend">
                                <button class="btn btn-success"><i class="fas fa-search icon"></i></button>
                            </div>  --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card h-100 list-toko position-relative">

                </div>
                <div class="spin-load d-none">
                    <span class="spinner-border text-primary text-center"></span>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            let slug = "{{ $request->slug }}";
            $('.spin-load').removeClass('d-none');
            $.ajax({
                type: 'GET',
                url: "{{ url('aj-toko/render-data') }}",
                data: {
                    _token:"{{ csrf_token() }}",
                    slug:slug
                },
                success: function(res){
                    $('.list-toko').html(res);
                    $('.spin-load').addClass('d-none');
                }
            });
            $('select[name="ampas_provinsi"]').change(function(){
                let data = {
                    provincie_id: $(this).val(),
                    city_id: '',
                    harga: ''
                }
                $('.spin-load').removeClass('d-none');
                filterdata(data.provincie_id);
            });
            $('select[name="ampas_kota"]').change(function(){
                let data = {
                    provincie_id: '',
                    city_id: $(this).val(),
                    harga: ''
                }
                $('.spin-load').removeClass('d-none');
                filterdata(data.provincie_id,data.city_id);

            });
            $('input[name="ampas_harga"]').change(function(){
                let data = {
                    provincie_id: '',
                    city_id: '',
                    harga: $(this).val()
                }
                $('.spin-load').removeClass('d-none');
                filterdata(data.provincie_id,data.city_id,data.harga);

            });
            function filterdata(provincie_id,city_id,harga)
            {
                $.ajax({
                    type: 'POST',
                    url: "{{ url('aj-toko/filter-data') }}",
                    data: {
                        _token:"{{ csrf_token() }}",
                        provincie_id:provincie_id,
                        city_id:city_id,
                        harga:harga
                    },
                    success: function(res){
                        $('.list-toko').html(res);
                        $('.spin-load').addClass('d-none');
                    }
                });
            }
            selectTwo();
        });
    </script>
@endsection
