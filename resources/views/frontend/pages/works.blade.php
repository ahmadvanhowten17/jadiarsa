@extends('frontend.layouts.default')

@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">Works</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-body">
                        <img src="img/Picture5.png" class="img-fluid" alt="" srcset="">
                    </div>
                    <div class="card-images">
                        <img src="img/Picture2.png" class="img-fluid" alt="" srcset="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-body">
                        <img src="img/Picture7.png" class="img-fluid" alt="" srcset="">
                    </div>
                    <div class="card-images">
                        <img src="img/Picture3.png" class="img-fluid" alt="" srcset="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-work-desain">
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h2 class="text-center mt-5">Works Desain</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body position-relative">
                        <img src="img/Picture5.png" class="img-fluid" alt="" srcset="">
                        <span class="span-img"></span>
                        <div class="title-design">
                            <p class="">Design</p>
                            <ul>
                                <li>RUMAH
                                    <OL>
                                        <li>RUMAH TYPE 36</li>
                                        <li>RUMAH TYPE 45</li>
                                        <li>RUMAH TYPE 60</li>
                                        <li>RUMAH TYPE 72</li>
                                        <li>RUMAH TYPE 90</li>
                                        <li>RUMAH TYPE 105</li>
                                    </OL>
                                </li>
                                <li>RUKO</li>
                                <li>DESIGN WITH ARCHITECT</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        @for ($i = 10; $i <=  15; $i++)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{ url("img/Picture$i.png") }}" alt="" srcset="" class="img-fluid" height="100%">
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-work-desain mt-5">
    <div class="container">
        <div class="card">
            <div class="card-title">
                <h2 class="text-center mt-5">Works Budgeting</h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-body position-relative">
                        <img src="{{ url('img/Picture7.png') }}" class="img-fluid" alt="" srcset="">
                        <span class="span-img"></span>
                        <div class="title-design">
                            <p class="">Budgeting</p>
                            <ul>
                                <li>Rab Bangunan
                                    <OL>
                                        <li>Lantai
                                            <p class="mb-0">- Rab Bangunan</p>
                                        </li>
                                        <li>Lantai
                                            <p class="mb-0">- Rab Bangunan</p>
                                        </li>
                                        <li>Lantai</li>
                                    </OL>
                                </li>
                                <li style="list-style: none">- Budgeting With</li>
                                <li style="list-style: none">Estimator</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row position-relative">
                        @for ($i = 1; $i <=  3; $i++)
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body">
                                        <img src="{{ url("img/desain$i.png") }}" alt="" srcset="" class="img-fluid" height="100%">
                                    </div>
                                </div>
                            </div>
                        @endfor
                        <div class="text-center">
                            <div class="budgeting">
                                <p>MAU DESIGN RUMAH SESUAI KEINGINAN ? <br> <a href="{{ url('/') }}">jadiarsi.com</a> bantu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

