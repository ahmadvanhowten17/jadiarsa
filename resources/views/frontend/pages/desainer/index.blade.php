@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">jadi-{{ $pageUrl }}er</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card" style="min-height: 30vh">
            <div class="card-title">
                <center><h2 class="text-bold">Jadi Desainer</h2></center>
            </div>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Verivikasi Email</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Jadi Desainer</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="card-body">
                            <center>
                                <a href="javascript:void(0)" class="btn btn-success btn-veriv {{ $verif || auth()->user()->status_veriv === 'verived' ?  'd-none' : '' }}" data-url="{{ url('jadi-punya-toko') }}">Verivikasi Email Anda</a>
                                <div class="input-group w-25 mt-3 {{ $verif ?  '' : 'd-none' }}">
                                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Masukan kode verivikasi">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-primary btn-kode" data-url="{{ url('jadi-punya-toko') }}" data-id="{{ Auth::id() }}">Kirim</button>
                                    </div>
                                </div>
                            </center>

                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="card-body">
                        @if ($lapak)
                            <center><a href="javascript:void(0)" class="btn btn-success {{ auth()->user()->status_veriv === null ? 'disabled' : 'add-desain' }}" data-url="{{ url("$pageUrl/create") }}" data-title="Add Desain" data-id="{{ $lapak->id or '' }}">add Desain</a></center>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-deskripsi" style="margin-top: -150px">
    <div class="container">
        <div class="card">
            <div class="list-product"></div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="form-modal">

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            var url = "{{ url($pageUrl) }}/"+ 'list-desain';
            listProduct(url);
        });
    </script>
@endsection




















