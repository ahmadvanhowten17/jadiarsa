<div class="modal-body">
    <form id="dataFormModal" action="{{ url("desain") }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="lapak_id" value="{{ $id_lapak }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="">Kategori desain</label>
                    <select name="kategori_interior_id" class="form-control select2" style="width: 100%;" data-url="{{ url('kategori') }}">
                        @foreach ($kategoriDesain as $kat)
                            <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="">Type desain</label>
                    <select name="type_interior" class="form-control select2" style="width: 100%;" data-url="{{ url('kategori') }}">
                        <option value="A">Type A</option>
                        <option value="B">Type B</option>
                        <option value="C">Type C</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nama Desain</label>
                    <input name="nama_interior" type="text" class="form-control" placeholder="Nama Desain">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Jumlah Lantai</label>
                    <input name="jumlah_lantai" type="text" class="form-control" placeholder="Jumlah Lantai">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Estimasi Pengerjaan:</label>

                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" name="estimasi" class="form-control float-right" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group"><label for="">Jumlah Ruangan</label>
                    <input type="text" class="form-control" name="jumlah_ruangan">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Deskripsi Barang</label>
            <textarea name="deskripsi_interior" cols="30" rows="10" class="form-control summernote"></textarea>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Harga Normal</label>
                    <input name="harga_normal" type="text" class="form-control change-money">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Disc Interior</label>
                    <input name="disc_interior" type="text" class="form-control disc" placeholder="masukan Tanpa %">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Sub Total</label>
            <input name="harga_interior" type="text" class="form-control sub-total" readonly>
        </div>
        <div class="form-group">
            <label for="">Upload Photo Barang</label>
            <input name="atachment[]" type="file" class="form-control" multiple>
            <span class="text-danger">Upload image lebih dari satu data</span>
        </div>
        <center><h3>Pilih Alamat Anda</h3></center><hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Provinsi</label>
                        <select name="provincie_id" class="select2 form-control data-provin" data-url="{{ url('myprofile') }}">
                            <option value="">Pilih Provinsi</option>
                            @foreach ($provinsi as $prov)
                                <option value="{{ $prov->id }}">{{ $prov->provinsi }}</option>
                            @endforeach
                        </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Pilih Kec/Kab</label>
                    <select name="citie_id" id="list-kota" class="form-control select2">
                        <option value="">Pilih Kec/Kab</option>
                    </select>
                </div>
            </div>
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl/jadi-desainer") }}">Save changes</button>
    </div>
