@if ($record->count() > 0)
    <h2 class="">List Desain</h2>
    <div class="slide owl-carousel owl-theme">
        @foreach ($record as $item)
            <div class="item p-2 position-relative punya-toko">
                <img src="{{ url('storage/'.$item->attachment()->first()->url) }}" alt="" class="card-img-top" height="170px">
                <div class="card-body" style="background-color: #000000; color: #ffffff !important">
                    <h5 class="card-title text-center">{{ $item->nama_interior }}</h5>
                    <p class="card-text text-center">Rp. {{ number_format($item->harga_interior,'2',',','.') }}</p>
                </div>
                <div class="action text-center">
                    <button class="btn btn-warning btn-show" data-id="{{ $item->id }}" data-url="{{ url($pageUrl.'/show') }}" data-title="Show Barang"><i class="fas fa-eye"></i></button>
                    <a href="javascript:void(0)" data-url="{{ url("$pageUrl/$item->id/edit") }}" class="btn btn-success btn-edit" data-title="Edit Barang"><i class="fas fa-edit"></i></a>
                    <button class="btn btn-danger btn-delete" data-url="{{ url("$pageUrl/$item->id") }}"><i class="fas fa-trash"></i></button>
                </div>
            </div>
        @endforeach
    </div>
@endif






























