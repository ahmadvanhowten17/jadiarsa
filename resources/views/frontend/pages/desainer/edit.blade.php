<div class="modal-body">
    <form id="dataFormModal" action="{{ url("$pageUrl/$record->id") }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <input type="hidden" name="id" value="{{ $record->id }}">
        <input type="hidden" name="lapak_id" value="{{ $record->lapak_id }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="">Kategori desain</label>
                    <select name="kategori_interior_id" class="form-control select2" style="width: 100%;" data-url="{{ url('kategori') }}">
                        @foreach ($kategoriDesain as $kat)
                            <option value="{{ $kat->id }}" {{ $record->kategori_interior === $kat->id ? 'selected' : '' }}>{{ $kat->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="" class="">Type desain</label>
                    <select name="type_interior" class="form-control select2" style="width: 100%;" data-url="{{ url('kategori') }}">
                        <option value="A" {{ $record->type_interior == 'A' ? 'selected' : '' }}>Type A</option>
                        <option value="B" {{ $record->type_interior == 'B' ? 'selected' : '' }}>Type B</option>
                        <option value="C" {{ $record->type_interior == 'C' ? 'selected' : '' }}>Type C</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Nama Desain</label>
                    <input name="nama_interior" type="text" class="form-control" value="{{ $record->nama_interior }}" placeholder="Nama Desain">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Jumlah Lantai</label>
                    <input name="jumlah_lantai" type="text" class="form-control" value="{{ $record->jumlah_lantai }}" placeholder="Jumlah Lantai">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Estimasi Pengerjaan:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="far fa-calendar-alt"></i>
                        </span>
                      </div>
                      <input type="text" name="estimasi" class="form-control float-right" value="{{ $record->estimasi }}" id="reservation">
                    </div>
                    <!-- /.input group -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group"><label for="">Jumlah Ruangan</label>
                    <input type="text" class="form-control" name="jumlah_ruangan" value="{{ $record->jumlah_ruangan }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Deskripsi Barang</label>
            <textarea name="deskripsi_interior" cols="30" rows="10" class="form-control summernote">{!! $record->deskripsi_interior !!}</textarea>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Harga Normal</label>
                    <input name="harga_normal" type="text" class="form-control change-money" value="{{ $record->harga_normal }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Disc Interior</label>
                    <input name="disc_interior" type="text" class="form-control disc" value="{{ $record->disc_interior }}" placeholder="masukan Tanpa %">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Sub Total</label>
            <input name="harga_interior" type="text" class="form-control sub-total" value="Rp.{{ number_format($record->harga_interior,'2',',','.') }}" readonly>
        </div>
        <div class="form-group">
            <label for="">Upload Photo Barang</label>
            <input name="atachment[]" type="file" class="form-control" multiple>
            <span class="text-danger">Upload image lebih dari satu data</span>
        </div>
        <div class=" mb-2">
            <div class="row">
                @foreach ($record->attachment as $item)
                    <div class="col-md-2">
                        <img src="{{ url('storage/'.$item->url) }}" class="img-fluid" width="150" alt="">
                    </div>
                @endforeach
            </div>
        </div>
        <center><h3>Pilih Alamat Anda</h3></center><hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Provinsi</label>
                    <select name="provin_id" id="provins" class="select2 form-control data-provin" data-url="{{ url('myprofile') }}">
                        <option value="">Pilih Provinsi</option>
                        @foreach ($provinsi as $item)
                            <option value="{{ $item->id }}" {{ $item->id === $record->provincie_id ? 'selected' : '' }}>{{ $item->provinsi }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Kabupaten / Kota</label>
                    <select name="city_id" id="list-kota" class="select2 form-control">
                        <option value="">Pilih Kabupaten / Kota</option>
                        @if ($record->citie_id)
                            @foreach ($kota->where('provin_id',$record->provincie_id)->get() as $item)
                                <option value="{{ $item->id }}" {{ $item->id === $record->citie_id ? 'selected' : '' }}>{{$item->type .' '. $item->kota }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary btn-simpan"data-url="{{ url("$pageUrl") }}">Save changes</button>
    </div>
