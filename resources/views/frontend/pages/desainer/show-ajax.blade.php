<div class="modal-body">
    <div class="card">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <img src="{{ $record->attachment ? url('storage/'.$record->attachment()->first()->url) : '' }}" alt="" class="card-img-top product" height="320">
                <div class="row">
                    @foreach ($record->attachment as $image)
                        <div class="col-2 col-md-3 col-sm-6 mt-2 pr-0">
                            <a href="javascript:void(0)" class="">
                                <img src="{{ url('storage/'.$image->url) }}" alt="" class="show-img" width="100%" height="50px">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <h3 class="my-3 text-capitalize">{{ $record->nama_interior }}</h3>
                @for ($i = 1; $i <= 5; $i++)
                    <span><i class="fa fa-star" style="color:#ff7429;"></i></span>
                @endfor
                <span>5 Ulasan</span>
                <span>1 Terjual</span>
                {{-- <table class="table table-borderless">
                    <tr>
                        <th width="150px">Stok Barang <span class="float-right">:</span></th>
                        <td>{{ $record->stock_barang ?? '0'}}</td>
                    </tr>
                    <tr>
                        <th width="150px">Berat Barang <span class="float-right">:</span></th>
                        <td>{{ $record->berat_barang ?? '0'}} (gr)</td>
                    </tr>
                    <tr>
                        <th width="150px">Terjual <span class="float-right">:</span></th>
                        <td>{{ $record->barang_terjual ?? '0'}}</td>
                    </tr>
                    <tr>
                        <th width="150px">Nama Toko <span class="float-right">:</span></th>
                        <td>{{ $record->lapaks->nama_toko }}</td>
                    </tr>
                    <tr>
                        <th width="150px">Alamat Toko <span class="float-right">:</span></th>
                        <td>{{ $record->lapaks->provincies->provinsi.' '.$record->lapaks->cities->kota ?? '' }}</td>
                    </tr>
                </table> --}}
                <h4 class="mt-4" style="font-weight: bold">
                    Rp. {{ number_format($record->harga_interior,'2',',','.') }}
                </h4>
            </div>
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
</div>
