
<div class="card">
    <div class="card-title">
        <h3 class="mt-2 ml-2">Barang Terkait</h3>
    </div>
    <div class="slide owl-carousel owl-theme">
        @foreach ($record as $item)
            @if ($item->id != $id)
                <div class="item p-2">
                    <img src="{{ url('storage/'.$item->attachment()->orderBy('created_at','asc')->first()->url) }}" alt="" class="card-img-top" height="170px">
                    <div class="card-body px-0" style="height: 100px !important">
                        <a href="{{ url("p/$item->slug") }}" class="" style="list-style: none; color: #000000; font-size: 18px; font-weight: bold !important"><span>{{ $item->nama_barang }}</span></a>
                        <p class="card-text">Rp. {{ number_format($item->harga_barang,'2',',','.') }}</p>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
