<div class="card-header">
    <img src="{{ url($user['photoURL']) }}" alt="" class="profile-pic rounded-circle">
    <span>{{ $user['username'] }}</span>
</div>
<ul class="list-group list-group-flush" id="listChat">
</ul>
