<span id="key" class="key d-none" data-key="{{ $chatkey }}"></span>
@if ($message)
    @foreach ($message as $item)
        @if ($item['userId'] !== auth()->user()->user_key)
            <div class="row">
                <div class="col-2 col-sm-1 col-md-1">
                    <img src="{{ url($frienphoto) }}" class="chat-pic rounded-circle" alt="" srcset="" width="50" height="50">
                </div>
                <div class="col-6 col-sm-6 col-md-6">
                    <p class="recive">{{ $item['msg'] }}
                    <span class="time float-right">{{ $item['dateTime'] }}</span>
                    </p>
                </div>
            </div>
        @else
            <div class="row justify-content-end">
                <div class="col-6 col-sm-6 col-md-6">
                    <p class="sent float-right">{{ $item['msg'] }}
                    <span class="time float-right">{{ $item['dateTime'] }}</span>
                    </p>
                </div>
                <div class="col-2 col-sm-1 col-md-1">
                    <img src="{{ (auth()->user()->pictures()->orderBy('created_at','desc')->first()) ? url('storage/'.auth()->user()->pictures()->orderBy('created_at','desc')->first()->url) : url('user.png') }}" class="chat-pic rounded-circle" alt="" srcset="" height="50" width="50">
                </div>
            </div>
        @endif
    @endforeach
    @else
@endif
