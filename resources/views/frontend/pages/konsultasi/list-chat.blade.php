<li class="list-group-item" style="background-color: #f8f8f8">
    <input type="text" placeholder="Search Nerw Chat" class="form-control form-rounded">
</li>
@foreach ($chat as $key => $item)
    <li class="list-group-item list-group-item-action btn-friend" data-key="{{ $datakey[$key] }}" data-url="{{ url($pageUrl.'/add-friend') }}" data-name="{{ $item['username'] }}" data-image="{{ $item['photoURL'] }}">
        <div class="row">
            <div class="col-md-2">
                <img src="{{ $item['photoURL'] }}" id="image" class="profile-pic rounded-circle img-friend" alt="" srcset="">
            </div>
            <div class="col-md-10" style="cursor: pointer">
                <div class="name">{{ $item['username'] }}</div>
                <div class="under-name">This is some mesage text ...</div>
            </div>
        </div>
    </li>
@endforeach
