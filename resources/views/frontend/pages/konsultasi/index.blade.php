@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">{{ $pageUrl }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="card-title">
                <center><h2 class="text-bold">Konsultasi Dengan Arsitek Kami</h2></center>
            </div>
            <div class="row">
                <div class="col-md-4 chat-title">
                    <center>
                        <span class="spinner-border text-primary text-center"></span>
                    </center>
                </div>
                <div class="col-md-8 pl-0" id="side-2">
                    <center class="my-5 d-none friend-load">
                        <span class="spinner-border text-primary text-center"></span>
                    </center>
                    <div id="chatpanel" class="card h-100" style="display: none";>
                        <div class="card-header">
                            <div class="row">
                                <div class="col-1 col-sm-1 col-md-1 col-lg-1">
                                    <i class="fas fa-list mt-2" style="cursor: pointer"></i>
                                </div>
                                <div class="col-2 col-sm-2 col-md-2 col-lg-1">
                                    <img src="{{ url('user.png') }}" class="profile-pic rounded-circle" id="imgChat" alt="" srcset="" width="50" height="50">
                                </div>
                                <div class="col-5 col-sm-5 col-md-5 col-lg-7">
                                    <div class="name" id="divChatName" data-key="">Any Name</div>
                                    <div class="under-name" id="divChatSeen">Last seen 20/12/2020</div>
                                </div>
                                <div class="col-4
                                 col-sm-4 col-md-4 col-lg-3 icon">
                                    <i class="fas fa-search"></i>
                                    <i class="fas fa-paperclip ml-4"></i>
                                    <i class="fas fa-ellipsis-v ml-4"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="message">
                            {{-- <div class="row">
                                <div class="col-2 col-sm-1 col-md-1">
                                    <img src="{{ asset('users.png') }}" class="chat-pic" alt="" srcset="">
                                </div>
                                <div class="col-6 col-sm-6 col-md-6">
                                    <p class="recive">this is pesan anda
                                        <span class="time float-right">1:23 pm</span>
                                    </p>
                                </div>
                            </div> --}}
                            {{-- <div class="row justify-content-end">
                                <div class="col-6 col-sm-6 col-md-6">
                                    <p class="sent float-right">this is pesan anda
                                        <span class="time float-right">1:23 pm</span>
                                    </p>
                                </div>
                                <div class="col-2 col-sm-1 col-md-1">
                                    <img src="{{ asset('users.png') }}" class="chat-pic" alt="" srcset="">
                                </div>
                            </div> --}}
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-2 col-md-1">
                                    <i class="far fa-grin fa-2x"></i>
                                </div>
                                <div class="col-8 col-md-10">
                                    <input id="txmessage" type="text" class="form-control form-rounded" placeholder="kirim Pesan" data-url="{{ url($pageUrl.'/start-chat') }}">
                                </div>
                                {{-- <div class="col-2 col-md-1">
                                    <div class="fas fa-microphone fa-2x"></div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divstart">
                        @if ($all_chat)
                            @foreach ($all_chat as $key => $item)
                                @if ($item['email'] !== auth()->user()->email && $item['status'] == 'admin')
                                    <div class="col-md-6 my-5">
                                        <div id="" class="row no-gutters bg-secondary">
                                            <div class="col-md-3 text-center">
                                                <img src="{{ url($item['photoURL']) }}" alt="" class="profile-pic rounded-circle my-3 img-friend">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h2 class="card-title text-capitalize">{{ $item['username'] }}</h2>
                                                    <p class="card-text">{{ $item['status'] }}</p>
                                                </div>
                                            </div>
                                            <div class="card-footer w-100">
                                                <button class="btn btn-success float-right btn-friend" data-key="{{ $key }}" data-url="{{ url($pageUrl.'/add-friend') }}" data-name="{{ $item['username'] }}">Chat</button>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="form-modal">

      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
     $(document).ready(function(){
        let url = "{{ url($pageUrl) }}";
        konsultasi(url);
        listchat(url);
    });
</script>
@endsection




















