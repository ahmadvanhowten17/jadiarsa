@extends('frontend.layouts.default')
@section('style')
    <style>
        .whatsup{
            display: none !important;
        }
    </style>
@endsection
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">{{ $bredcrumb }}</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                <div class="col-md-5 col-sm-12 ml-3 pl-0">
                    <img src="{{ url('storage/'.$record->attachment()->first()->url) }}" alt="" class="card-img-top product" height="320" data-id="{{ $record->kategori_id }}">
                    <div class="row">
                        @foreach ($record->attachment as $image)
                            <div class="col-2 col-md-3 col-sm-6 mt-2 pr-0">
                                <a href="javascript:void(0)" class="">
                                    <img src="{{ url('storage/'.$image->url) }}" alt="" class="show-img" width="100%" height="50px">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h3 class="my-3 text-capitalize">{{ $record->nama_barang }}</h3>
                    @for ($i = 1; $i <= 5; $i++)
                        <span><i class="fa fa-star" style="color:#ff7429;"></i></span>
                    @endfor
                    <span>5 Ulasan</span>
                    <span>1 Terjual</span>
                    <h4 class="mt-4" style="font-weight: bold">
                        Rp. {{ number_format($record->harga_barang,'2',',','.') }}
                    </h4>
                    <div class="row cek-ongkir">
                        <div class="col-md-3">
                            <span>Pengiriman</span>
                        </div>
                        <div class="col-md-9 position-relative">
                            <div class="d-flex">
                                <span class="text-ongkir">Lokasi Toko</span>
                                <span class="text-ongkir">Tujuan Pengiriman</span>
                            </div>
                            <div class="d-flex">
                                <p>{{ $record->lapaks->cities->kota }}</p>
                                <p class="pl-3 kota">Kota Bandung</p>
                                <span class="ubah-alamat">Ubah Alamat</span>
                            </div>
                            <div class="d-flex estimasi">
                                @foreach ($ongkir as $ongkirs)
                                    <p>Rp. {{ number_format($ongkirs['value'],'2',',','.') }}</p>
                                    <p>{{ $ongkirs['etd'] }} Hari</p>
                                @endforeach
                            </div>
                            <div class="lokasi-ongkir">
                                <select name="" id="input-lokasi" class="form-control" data-url="{{ url($pageUrl) }}" data-id="{{ $record->lapaks->citie_id }}" data-weight="{{ $record->berat_barang }}">
                                    @foreach ($kota->get() as $kotas)
                                        <option value="{{ $kotas->id }}" {{ $kotas->id === 23 ? 'selected' : '' }}>{{ $kotas->type.' '.$kotas->kota }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="add-cart">
                        <div class="btn btn-primary">
                          <i class="fas fa-cart-plus fa-lg mr-2"></i>
                          Add to Cart
                        </div>

                        <div class="btn btn-danger">
                          <i class="fas fa-heart fa-lg mr-2"></i>
                          Add to Wishlist
                        </div>
                        <a href="https://web.whatsapp.com/send?phone=6282218422670&text=Halo%20Jadiarsi%2C%20saya%20ingin%20bertanya%20terkait%20produk%20{{ url($pageUrl.'/'.$bredcrumb) }}" class="btn btn-success">
                          <i class="fab fa-whatsapp fa-lg mr-2"></i>
                          Chat Whatsapp
                        </a>
                    </div>
                    <div class="mt-1 product-share">
                        <span class="my-3">Share : </span>
                        <a href="#" class="text-gray">
                          <i class="fab fa-facebook-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                          <i class="fab fa-twitter-square fa-2x"></i>
                        </a>
                        <a href="#" class="text-gray">
                          <i class="fas fa-envelope-square fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-deskripsi mt-3">
    <div class="container">
        <div class="card">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Deskripsi Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Informasi Barang</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="ml-3 mt-3">
                        {!! $record->deskripsi_barang !!}
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table table-borderless">
                        <tr>
                            <th width="150px">Stok Barang <span class="float-right">:</span></th>
                            <td>{{ $record->stock_barang ?? '0'}}</td>
                        </tr>
                        <tr>
                            <th width="150px">Berat Barang <span class="float-right">:</span></th>
                            <td>{{ $record->berat_barang ?? '0'}} (gr)</td>
                        </tr>
                        <tr>
                            <th width="150px">Terjual <span class="float-right">:</span></th>
                            <td>{{ $record->barang_terjual ?? '0'}}</td>
                        </tr>
                        <tr>
                            <th width="150px">Nama Toko <span class="float-right">:</span></th>
                            <td>{{ $record->lapaks->nama_toko }}</td>
                        </tr>
                        <tr>
                            <th width="150px">Alamat Toko <span class="float-right">:</span></th>
                            <td>{{ $record->lapaks->provincies->provinsi.' '.$record->lapaks->cities->kota ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-work-desain mt-3">
    <div class="container list-product">

    </div>
</section>
<section class="section-cart">
    <a href="javascript:void(0)" class="btn-cart">
        <i class="fas fa-cart-plus fa-lg mr-2"></i>
        Add to Cart
    </a>
    <a href="https://web.whatsapp.com/send?phone=6282218422670&text=Halo%20Jadiarsi%2C%20saya%20ingin%20bertanya%20terkait%20produk%20{{ url($pageUrl.'/'.$bredcrumb) }}" class="btn-pesan">
        <i class="fab fa-whatsapp fa-lg mr-2"></i>
        Chat
    </a>
</section>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            var url = "{{ url($pageUrl) }}/"+'list/barang-terkait';
            var id = $('.product').data('id');
            var idBarang = "{{ $record->id }}";
            listProduct(url,id,idBarang);
        });
    </script>
@endsection
