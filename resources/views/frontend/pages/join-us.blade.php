@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">Join-us</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                @for ($i = 1; $i <= 3; $i++)
                    <div class="col-md-4 position-relative">
                        <div class="card-body">
                            <img src="{{ url("img/shop$i.png") }}" class="img-fluid" alt="" srcset="">
                        </div>
                        <span class="shop-abs"></span>
                    </div>
                @endfor

                <div class="shop-card">
                    <h4 class="text-center">MAU JADI KLIEN ? JADI DESAINER ? JADI SUPPLIER/DISTRIBUTOR ?<br>
                        YUK GABUNG Sini DI <a href="{{ url('/') }}">jadiarsi.com</a></h4>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection





















