@extends('frontend.layouts.default')
@section('content')
<section class="section-header-work"></section>
<section class="section-work">
    <div class="container">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Jadiarsi</li>
                        <li class="breadcrumb-item active">about</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="card">
            <div class="row">
                <div class="col-md-6 position-relative pr-0">
                    <img src="{{ url('img/gedung.png') }}" class="img-fluid h-100" alt="" srcset="">
                    <div class="about-abs">
                        <div class="d-flex">
                            <div class="flex-coloumn">
                                @for ($i = 1; $i <= 3; $i++)
                                    <img src="{{ url("img/about$i.png") }}" class="img-fluid" alt="">
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h1 class="card-title">About</h1><hr><hr>
                    <div class="card-body">
                        <p>
                            Jadiarsi.com merupakan situs web yang memudahkan konsumen dalam melakukan perencanaan dan pembangunan bangunan impiannya.
                        </p>
                    </div>
                    <h1 class="card-title mt-5">Lini bisnis kami</h1><hr><hr>
                    <div class="card-body">
                        <p>
                            Selain menjadi penyedia layanan perencanaan dan pembangunan, jadiarsi.com juga menawarkan produk-produk seperti :
                        </p>
                        <div class="d-flex">
                            @for ($i = 1; $i <= 3; $i++)
                                <div class="col-md-4">
                                    <img src="{{ url("img/about$i.png") }}" class="img-fluid" alt="">
                                    @if ($i === 1)
                                        <p class="text-center">Furniture</p>
                                    @elseif($i === 2)
                                        <p class="text-center">Elektronik</p>
                                    @elseif($i === 3)
                                        <p class="text-center">Alat Kontruksi</p>
                                    @endif
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection























