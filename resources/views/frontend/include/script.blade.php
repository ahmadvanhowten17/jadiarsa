<script src="{{ asset('library/jquery/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('library/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
@yield('script-fileInput')
<script src="{{ url('plugins/jquery/jquery.form.min.js') }}"></script>
<script src="{{ url('plugins/moment/moment.min.js') }}"></script>
<script src="{{ url('library/autonumerik/autoNumeric.js') }}"></script>
<script src="{{ url('plugins/summernote/summernote.min.js') }}"></script>
<script src="{{ url('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/owlcarousel/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
    $(document).ready(function() {
        // jQuery code

        //////////////////////// Prevent closing from click inside dropdown
        $(document).on('click', '.dropdown-menu', function (e) {
          e.stopPropagation();
        });

        // make it as accordion for smaller screens
        if ($(window).width() < 992) {
              $('.dropdown-menu a').click(function(e){
                  e.preventDefault();
                if($(this).next('.submenu').length){
                    $(this).next('.submenu').toggle();
                }
                $('.dropdown').on('hide.bs.dropdown', function () {
                   $(this).find('.submenu').hide();
                })
              });
        }

    }); // jquery end
</script>
@yield('script')













