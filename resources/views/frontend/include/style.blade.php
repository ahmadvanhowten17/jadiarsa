<link rel="stylesheet" href="{{ asset('library/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('library/bootstrap/css/bootstrap.min.css') }}">
@yield('fileInput')
<link href="https://fonts.googleapis.com/css2?family=Assistant&family=Playfair+Display&display=swap"
rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/owlcarousel/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/owlcarousel/css/owl.theme.default.css') }}">
<link rel="stylesheet" href="{{ url('plugins/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ url('plugins/sweetalert/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ url('plugins/summernote/summernote.min.css') }}">
<link rel="stylesheet" href="{{ asset('style/main.css') }}">
@yield('style')
