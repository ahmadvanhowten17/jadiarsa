<nav class="navbar navbar-expand-lg navbar-light bg-light position-relative">
    <a class="navbar-brand text-nav" href="{{ url('/') }}">
        <img src="{{ url('logo.jpeg') }}" alt="" srcset="" style="width: 60px; height: 40px">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-nav" href="#" data-toggle="dropdown" >
                Jasa Konsultasi
                </a>
                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)" class="dropdown-item">Desain Bangunan</a>
                        <ul class="submenu dropdown-menu">
                            <li><a class="dropdown-item" href="{{ url('/desain') }}">Contoh Desain</a></li>
					        <li><a class="dropdown-item" href="{{ url('/konsultasi') }}">Konsultasi Dengan Arsitek</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Desain Interior</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">Rencana Anggaran Biaya</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-nav" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Toko Bangunan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ url('cat-toko/toko-furniture') }}">Toko Furniture</a>
                    <a class="dropdown-item" href="{{ url('cat-toko/toko-material') }}">Toko Material</a>
                    <a class="dropdown-item" href="{{ url('cat-toko/toko-alat-dan-perlengkapan') }}">Toko Alat Dan Perlengkapan</a>
                    <a class="dropdown-item" href="{{ url('cat-toko/toko-alat-appliances') }}">Toko Home Appliances</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-nav" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Join Us
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ url('jadi-desainer') }}">Jadi Desainer</a>
                    <a class="dropdown-item" href="{{ url('jadi-punya-toko') }}">Jadi Punya Toko</a>
                    <a class="dropdown-item" href="#">Jadi Konsumen</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        </form>
        @if (!Auth::check())
        <a href="{{ url('login') }}" class="btn btn-success mr-2">Masuk</button>
        <a href="{{ url('register') }}" class="btn btn-secondary">Daftar</a>
        @else
        <div class="dropdowns">
            <a href="javascript:void(0)" class="dropdowns-toggle" data-title="dropdowns">
                <img src="{{ (auth()->user()->pictures()->orderBy('created_at','desc')->first()) ? url('storage/'.auth()->user()->pictures()->orderBy('created_at','desc')->first()->url) : url('user.png') }}" class="img-fluid" style="height: 30px; width: 30px; border-radius: 30px" title="{{ auth()->user()->username }}" alt="" srcset="">
            </a>
        </div>
        @endif
    </div>
</nav>
<ul class="dropdowns-menu">
    @if (Auth::check())
        @if (auth()->user()->status === 'admin')
            <li>
                <a href="{{ url('admin') }}" class="dropdown-item">Admin</a>
            </li>
        @endif
    @endif
    <li>
        <a href="{{ url('myprofile') }}" class="dropdown-item">Profile</a>
    </li>
    <li>
        <a class="dropdown-item" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
    </li>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</ul>
