<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ url('logo.jpeg') }}" rel="shortcut icon">
<title>{{ env('APP_NAME') ?? '' }}</title>
    @include('frontend.include.style')
</head>
<body>
    <main>
        @yield('content')
    </main>

    @include('frontend.include.script')
</body>
</html>
