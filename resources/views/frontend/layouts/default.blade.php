<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ url('logo.jpeg') }}" rel="shortcut icon">
<title>{{ env('APP_NAME') ?? '' }}</title>
    @include('frontend.include.style')
</head>
<body>
    <span class="whatsup fixed-bottom" id="mybuton">
        <a href="https://api.whatsapp.com/send?phone=6282218422670"><i class="fab fa-whatsapp"></i></a>
    </span>
    <div class="container-fluid p-0">
        @include('frontend.include.navbar')
    </div>
    @yield('header')
    <main>
        @yield('content')
    </main>
    @include('frontend.include.footer')
    @include('frontend.include.script')
</body>
</html>
