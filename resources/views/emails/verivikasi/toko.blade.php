@component('mail::message')
Halo User

<h1>Yuk Verivikasi Email Akun Toko anda</h1>
<p>Sedikit Lagi akun Jadi Punya Toko anda akan aktif. Cukup memasukan kode verivikasi di bawah untuk mengaktifkan toko anda.</p>
<center><b><h1>{{ $verif->kode_veriv }}</h1></b></center>
<p>Mohon Jangan sebar luaskan kode ini kesiapapun , temasuk pihak yang mengatasnamakan Jadiarsi</p>
<p>Terimakasih</p>

Salam Hangat {{ config('app.name') }}
@endcomponent
