> Ini hanya informasi umum tentang apa yang sedang dikerjakan di Branch ini

## :memo: List Bug



- [x] Halaman Homepage
- [X] Fitur Tambah Toko 


0. PHP Must Be 7.4
1. Composer Install
2. set .env from .env.example
3. php artisan key:generate
4. php artisan migrate
5. php artisan db:seed
6. php artisan db:seed --class=WilayahSeeder
7. npm install

php artisan config:clear
php artisan config:cache
php artisan view:clear
php artisan route:clear
