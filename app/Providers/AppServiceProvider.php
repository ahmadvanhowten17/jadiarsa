<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //191
        Relation::morphMap([
            'img_barang' => 'App\Model\LapakBarang',
            'img_desain' => 'App\Model\DesainInterior',
            'img_kat_desain' => 'App\Model\KategoriInterior',
        ]);
    }
}
