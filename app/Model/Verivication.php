<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Verivication extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
