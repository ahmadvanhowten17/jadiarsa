<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class SubKategorie extends Model
{
    protected $fillable = [
        'kategori_id','nama','slug','created_by','updated_by'
    ];

    public function kategories()
    {
        return $this->belongsTo(Kategori::class,'kategori_id');
    }

    public function childkategories()
    {
        return $this->hasMany(ChildKategorie::class);
    }
    
    public function users()
    {
        return $this->belongsTo(User::class,'created_by');
    }
}
