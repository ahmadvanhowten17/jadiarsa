<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ChildKategorie extends Model
{
    protected $fillable = [
        'sub_kategorie_id','nama','slug','created_by','updated_by'
    ];

    public function subkategori()
    {
        return $this->belongsTo(SubKategorie::class,'sub_kategorie_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
