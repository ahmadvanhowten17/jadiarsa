<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $fillable = [
        'name','slug','created_by','updated_by'
    ];

    public function subkategories()
    {
        return $this->hasMany(SubKategorie::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class,'created_by');
    }
}
