<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Attachment;
class DesainInterior extends Model
{
    protected $fillable = [
        'nama_interior','lapak_id','slug','deskripsi_interior','jumlah_ruangan',
        'disc_interior','type_interior','jumlah_lantai','harga_normal',
        'harga_interior','estimasi','kategori_interior_id',
        'provincie_id','citie_id','created_by','updated_by'
    ] ;

    public function attachment()
    {
        return $this->morphMany(Attachment::class,'target');
    }

    public function kategoridesain()
    {
        return $this->belongsTo(KategoriInterior::class,'kategori_interior_id');
    }

    public function lapak()
    {
        return $this->belongsTo(Lapak::class,'lapak_id');
    }

    public function city()
    {
        return $this->belongsTo(Citie::class,'citie_id');
    }
}
