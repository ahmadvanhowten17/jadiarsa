<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Citie extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
