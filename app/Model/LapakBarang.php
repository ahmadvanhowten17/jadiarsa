<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Attachment;

class LapakBarang extends Model
{
    protected $fillable = [
        'lapak_id','nama_barang',
        'slug','harga_barang',
        'deskripsi_barang','harga_normal',
        'berat_barang','disc_barang',
        'stock_barang','kondisi_barang',
        'kategori_id','sub_kategorie_id',
        'child_kategorie_id','merek',
        'status_barang','minimum_pembelian',
        'created_by','updated_by'
    ];

    public function attachment()
    {
        return $this->morphMany(Attachment::class,'target');
    }

    public function lapaks()
    {
        return $this->belongsTo(Lapak::class,'lapak_id');
    }

    public function kategories()
    {
        return $this->belongsTo(Kategori::class,'kategori_id');
    }

    public function SubKategories()
    {
        return $this->belongsTo(SubKategorie::class,'sub_kategorie_id');
    }
}
