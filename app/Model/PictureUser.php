<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PictureUser extends Model
{
    protected $fillable = [
        'name','url','user_id'
    ];

    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function getPhotoAttribute($value)
    {
        return url('storage/' .$value);
    }

    
}
