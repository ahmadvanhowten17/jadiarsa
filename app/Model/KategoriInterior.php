<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Attachment;
class KategoriInterior extends Model
{
    protected $guarded = [];


    public function attachment()
    {
        return $this->morphMany(Attachment::class,'target');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function desain()
    {
        return $this->hasMany(DesainInterior::class);
    }
}
