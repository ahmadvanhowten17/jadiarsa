<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Citie;
use App\Model\Provincie;
class Lapak extends Model
{
    protected $fillable = [
        'user_id','nama_toko','deskripsi_toko','alamat_toko','last_active',
        'phone','type_toko','provincie_id','citie_id','kode_pos','created_by','updated_by'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function provincies()
    {
        return $this->belongsTo(Provincie::class,'provincie_id');
    }

    public function cities()
    {
        return $this->belongsTo(Citie::class,'citie_id');
    }

    public function barangs()
    {
        return $this->hasMany(LapakBarang::class);
    }
}
