<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_interior' => 'required|max:100',
            'deskripsi_interior' => 'required|min:10',
            'harga_normal' => 'required',
            'estimasi' => 'required',
            'jumlah_lantai' => 'required|integer',
            'type_interior' => 'required',
            'kategori_interior_id' => 'required',
        ];
    }

    public function messages()
    {
      return [
        'required' => 'Kolom tidak boleh kosong',
        'min' => 'Isian kolom min :min angka',
        'max' => 'Isian kolom max :max angka',
        'integer' => 'Isian kolom harus berupa angka',
      ];
    }
}
