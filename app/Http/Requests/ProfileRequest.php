<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:100',
            'email'    => 'required',
            'gender'   => 'required',
            'alamat'   => 'required|max:150',
            'url'      => 'required|image|mimes:jpg,png|max:2048'

        ];
    }
}
