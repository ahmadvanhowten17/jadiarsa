<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Zipper;
class AdminUserController extends Controller
{
    protected $link = 'admin/user';
    public function __construct()
    {
        $this->setLink($this->link);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "text-center text-nowrap",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'username',
                'name' => 'username',
                'label' => 'Name',
                'searchable' => false,
                'sortable' => true,
                'width' => '20%',
                'className' => "text-left text-nowrap",

            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-left text-nowrap",

            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'updated_at',
                'name' => 'updated_at',
                'label' => 'Date',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ]
        ]);
    }

    public function showData(Request $request)
    {
        $records = User::select('*');
        // dd($request->all());
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
        // $records->->sort();
        $records->orderBy('created_at', 'desc');
        }
        //Filters
        if ($name = $request->name) {
        $records->where('username', 'like', '%'.$name.'%' );
        }
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('username', function ($record) {
                return '<td>'.$record->username.'</td>';
            })
            ->addColumn('email', function ($record) {
              return $record->email;
            })
            ->addColumn('status', function ($record) {
              return $record->status;
            })
            ->addColumn('updated_at', function ($record) {
                return $record->updated_at->diffForHumans();
            })
            ->addColumn('action', function ($records) {
                $btn = '';
                //Edit
                  $btn .= $this->makeButton([
                    'type' => 'detail',
                    'tooltip' => 'Lihat Data',
                    'id'   => $records->id,
                    'url' => url("admin/user/$records->id")
                  ]);

                  $btn .= $this->makeButton([
                    'type' => 'edit',
                    'tooltip' => 'Ubah Data',
                    'id'   => $records->id,
                    'url' => url("admin/user/$records->id/edit")
                  ]);
                  // Delete
                  $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $records->id,
                    'url' => url("admin/user/$records->id")
                  ]);

                return $btn;
              })
            ->rawColumns(['action','username'])
            ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render('backend.pages.user.index',[
            'active' => true,
            'mockup' => false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.pages.user.show',[
            'record' => User::find($id),
            'pageUrl' => $this->link
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('backend.pages.user.edit',[
            'record' => User::find($id),
            'pageUrl' => $this->link
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        try {
            $record = User::findOrFail($id);
            $record->update($data);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = User::find($id);
        $record->delete($id);
        return response([
            'status' => true,
            'url' => true
        ]);
    }
}
