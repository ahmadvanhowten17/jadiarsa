<?php

namespace App\Http\Controllers\Backend\Kategori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Kategori;
use Carbon\Carbon;
use Zipper;
class KategoriController extends Controller
{
    protected $link = 'admin/kategori';
    function __construct()
    {
        $this->setLink($this->link);

        // Header Grid Datatable
        $this->setTableStruct([
        [
            'data' => 'num',
            'name' => 'num',
            'label' => '#',
            'orderable' => false,
            'searchable' => false,
            'className' => "text-center text-nowrap",
            'width' => '40px',
        ],
        /* --------------------------- */
        [
            'data' => 'name',
            'name' => 'name',
            'label' => 'name',
            'searchable' => false,
            'sortable' => true,
            'width' => '20%',
            'className' => "text-left text-nowrap",

        ],
        [
            'data' => 'created_at',
            'name' => 'created_at',
            'label' => 'Date',
            'searchable' => false,
            'sortable' => true,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ],
        [
            'data' => 'username',
            'name' => 'username',
            'label' => 'Created By',
            'searchable' => false,
            'sortable' => true,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ],
        [
            'data' => 'action',
            'name' => 'action',
            'label' => 'Aksi',
            'searchable' => false,
            'sortable' => false,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ]
        ]);
    }
    public function showData(Request $request)
    {
        $records = Kategori::with('users')->select('*');
        // dd($request->all());
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
        // $records->->sort();
        $records->orderBy('created_at', 'desc');
        }
        //Filters
        if ($name = $request->name) {
        $records->where('name', 'like', '%'.$name.'%' );
        }
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('name', function ($record) {
                return '<td>'.$record->name.'</td>';
            })
            ->addColumn('created_at', function ($record) {
              return $record->created_at->diffForHumans();
            })
            ->addColumn('username', function ($record) {
              return $record->users->username;
            })
            ->addColumn('action', function ($records) {
                $btn = '';
                //Edit
                  $btn .= $this->makeButton([
                    'type' => 'detail',
                    'tooltip' => 'Lihat Data',
                    'id'   => $records->id,
                    'url' => url("admin/kategori/$records->id")
                  ]);

                  $btn .= $this->makeButton([
                    'type' => 'edit',
                    'tooltip' => 'Ubah Data',
                    'id'   => $records->id,
                    'url' => url("admin/kategori/$records->id/edit")
                  ]);
                  // Delete
                  $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $records->id,
                    'url' => url("admin/kategori/$records->id")
                  ]);

                return $btn;
              })
            ->rawColumns(['action','name'])
            ->make(true);
    }
    public function index()
    {
        return $this->render('backend.pages.kategori.index',[
            'active' => true,
            'mockup' => false
        ]);
    }

    public function show(Request $request, $id)
    {
        return view('backend.pages.kategori.show',[
            'pageUrl' => $this->link,
            'active' => true,
            'record' => Kategori::find($id)
        ]);
    }

    public function create()
    {
        return view('backend.pages.kategori.create',[
            'pageUrl' => $this->link,
            'active' => true
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        try {
            $data = Kategori::create([
                'name' => $request->name,
                'slug' => str::slug($request->name),
                'created_by' => Auth::id()
            ]);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

    public function edit($id)
    {
        $record = Kategori::findOrFail($id);
        return view('backend.pages.kategori.edit',[
            'pageUrl' => $this->link,
            'active' => true,
            'record' => $record
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $record = Kategori::findOrFail($id);
        try {
            $data = $record->update([
                'name' => $request->name,
                'slug' => str::slug($request->name),
                'updated_by' => Auth::id()
            ]);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

    public function delete($id)
    {
        $data = Kategori::findOrFail($id);
        $data->delete();
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

}
