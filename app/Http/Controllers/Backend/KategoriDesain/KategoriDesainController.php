<?php

namespace App\Http\Controllers\Backend\KategoriDesain;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\KategoriInterior;
use Carbon\Carbon;
class KategoriDesainController extends Controller
{
    protected $link = 'admin/kategori-desain';
    function __construct()
    {
        $this->setLink($this->link);

        // Header Grid Datatable
        $this->setTableStruct([
        [
            'data' => 'num',
            'name' => 'num',
            'label' => '#',
            'orderable' => false,
            'searchable' => false,
            'className' => "text-center text-nowrap",
            'width' => '40px',
        ],
        /* --------------------------- */
        [
            'data' => 'nama',
            'name' => 'nama',
            'label' => 'nama',
            'searchable' => false,
            'sortable' => true,
            'width' => '20%',
            'className' => "text-left text-nowrap",

        ],
        [
            'data' => 'created_at',
            'name' => 'created_at',
            'label' => 'Date',
            'searchable' => false,
            'sortable' => true,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ],
        [
            'data' => 'username',
            'name' => 'username',
            'label' => 'Created By',
            'searchable' => false,
            'sortable' => true,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ],
        [
            'data' => 'action',
            'name' => 'action',
            'label' => 'Aksi',
            'searchable' => false,
            'sortable' => false,
            'width' => '100px',
            'className' => "text-center text-nowrap",

        ]
        ]);
    }

    public function showData(Request $request)
    {
        $records = KategoriInterior::with('user')->select('*');
        // dd($request->all());
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
        // $records->->sort();
        $records->orderBy('created_at', 'desc');
        }
        //Filters
        if ($name = $request->name) {
        $records->where('nama', 'like', '%'.$name.'%' );
        }
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('nama', function ($record) {
                return '<td>'.$record->nama.'</td>';
            })
            ->addColumn('created_at', function ($record) {
              return $record->created_at->diffForHumans();
            })
            ->addColumn('username', function ($record) {
              return $record->user->username;
            })
            ->addColumn('action', function ($records) {
                $btn = '';
                //Edit
                  $btn .= $this->makeButton([
                    'type' => 'detail',
                    'tooltip' => 'Lihat Data',
                    'id'   => $records->id,
                    'url' => url("admin/kategori-desain/$records->id")
                  ]);

                  $btn .= $this->makeButton([
                    'type' => 'edit',
                    'tooltip' => 'Ubah Data',
                    'id'   => $records->id,
                    'url' => url("admin/kategori-desain/$records->id/edit")
                  ]);
                  // Delete
                  $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $records->id,
                    'url' => url("admin/kategori-desain/$records->id")
                  ]);

                return $btn;
              })
            ->rawColumns(['action','nama'])
            ->make(true);
    }

    public function index()
    {
        return $this->render('backend.pages.kategori-desain.index',[
            'active' => true,
            'mockup' => false
        ]);
    }

    public function create()
    {
        return $this->render('backend.pages.kategori-desain.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        try {
            $data = KategoriInterior::create([
                'nama' => $request->nama,
                'slug' => str::slug($request->nama),
                'created_by' => Auth::id()
            ]);
            if ($request->hasFile('atachment')) {
                $photo = $request->file('atachment');
                foreach($photo as $file){
                    $name   = $file->getClientOriginalName();
                    $url    =$file->store('asset/kategori-desain','public');
                    $data->attachment()->create([
                        'filename' => $name,
                        'url' => $url,
                    ]);
                }
            }
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }
}
