<?php

namespace App\Http\Controllers\Backend\SubKategori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\SubKategorie;
use App\Model\Kategori;
use Yajra\DataTables\DataTables;
use Zipper;
use Carbon\Carbon;

class SubKategoriController extends Controller
{
    protected $link = 'admin/subkategori';
    function __construct()
    {
        $this->setLink($this->link);

        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => "text-center text-nowrap",
                'width' => '40px',
            ],
            /* --------------------------- */
            [
                'data' => 'kategori_id',
                'name' => 'kategori_id',
                'label' => 'Nama Kategori',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama SubKategori',
                'searchable' => false,
                'sortable' => true,
                'width' => '20%',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Date',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'username',
                'name' => 'username',
                'label' => 'Created By',
                'searchable' => false,
                'sortable' => true,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '100px',
                'className' => "text-center text-nowrap",

            ]
        ]);
    }
    public function showData(Request $request)
    {
        $records = SubKategorie::with('kategories','users')->select('*');
        // dd($request->all());
        //Init Sort
        if (!isset(request()->order[0]['column'])) {
        // $records->->sort();
        $records->orderBy('created_at', 'desc');
        }
        //Filters
        if ($name = $request->name) {
        $records->where('nama', 'like', '%'.$name.'%' );
        }
        return DataTables::of($records)
            ->addColumn('num', function ($record) use ($request) {
                return $request->get('start');
            })
            ->addColumn('kategori_id', function ($record) {
                return '<span class="badge badge-primary">'.$record->kategories->name.'</span>';
            })
            ->addColumn('nama', function ($record) {
                return $record->nama;
            })
            ->addColumn('created_at', function ($record) {
              return $record->created_at->diffForHumans();
            })
            ->addColumn('username', function ($record) {
              return $record->users->username;
            })
            ->addColumn('action', function ($record) {
                $btn = '';
                //Edit
                  $btn .= $this->makeButton([
                    'type' => 'detail',
                    'tooltip' => 'Lihat Data',
                    'id'   => $record->id,
                    'url' => url("$this->link/$record->id")
                  ]);

                  $btn .= $this->makeButton([
                    'type' => 'edit',
                    'tooltip' => 'Ubah Data',
                    'id'   => $record->id,
                    'url' => url("$this->link/$record->id/edit")
                  ]);
                  // Delete
                  $btn .= $this->makeButton([
                    'type' => 'delete',
                    'id'   => $record->id,
                    'url' => url("$this->link/$record->id")
                  ]);

                return $btn;
              })
            ->rawColumns(['action','kategori_id'])
            ->make(true);
    }
    public function index()
    {
        return $this->render('backend.pages.subkategori.index',[
            'active' => true,
            'mockup' => false
        ]);
    }

    public function show($id)
    {
        return view('backend.pages.subkategori.show',[
            'pageUrl' => $this->link,
            'record' => SubKategorie::find($id),
            'active' => true
        ]);
    }

    public function create()
    {
        $record = Kategori::get();
        return view('backend.pages.subkategori.create',[
            'pageUrl' => $this->link,
            'record' => $record,
            'active' => true
        ]);
    }
    public function edit($id)
    {
        $kaegori = Kategori::get();
        return view('backend.pages.subkategori.edit',[
            'pageUrl' => $this->link,
            'record' => SubKategorie::findOrFail($id),
            'kategori' => $kaegori,
            'active' => true
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kategori_id' => 'required'
        ]);
        try {
            $data = SubKategorie::create([
                'kategori_id' => $request->kategori_id,
                'nama' => $request->nama,
                'slug' => str::slug($request->nama),
                'created_by' => Auth::id()
            ]);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

    public function update(Request $request , $id)
    {
        $request->validate([
            'nama' => 'required',
            'kategori_id' => 'required'
        ]);
        $record = SubKategorie::findOrFail($id);
        try {
            $data = $record->update([
                'kategori_id' => $request->kategori_id,
                'nama' => $request->nama,
                'slug' => str::slug($request->nama),
                'updated_by' => Auth::id()
            ]);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }

    public function delete($id)
    {
        $data = SubKategorie::findOrFail($id);
        $data->delete();
        return response([
            'status' => true,
            'url'    => 'asd',
        ]);
    }
}
