<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $link = "admin";
    public function index()
    {
        return view('backend.pages.dashboard.admin',[
            'active' => 'dashboard',
            'pageUrl' => $this->link,
            ]);
    }
}
