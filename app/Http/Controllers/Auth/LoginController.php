<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->status === 'admin') {
            return redirect('admin');
        }
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request, $provider)
    {
        $errors = [];
        if (!$request->has('code') || $request->has('denied')) {
            return redirect('/');
        }
        session()->put('state', request()->input('state'));
        $socialUser = Socialite::driver($provider)->user();

        $user = User::where('email', $socialUser->getEmail())->first();
        if($user == true){
            // dd('True');
            $this->guard()->login($user);
                return redirect(url('/'));
        }else{
            // dd('False',$socialUser);

            $user = User::where('provider_id', $socialUser->getID())->where('provider',$provider)->first();

            if(!$user){

                $user = User::create([
                    'username'    => $socialUser->getName(),
                    'email'       => is_null($socialUser->getEmail()) ? str_slug($socialUser->getName()) . '@mail.com' : $socialUser->getEmail(),
                    'provider'    => $provider,
                    'provider_id' => $socialUser->getID(),
                    'status'      => 'users',
                ]);

                $this->guard()->login($user);
                return redirect ('/');
            }else{
                $errors = ['Akun' => 'Akun Tidak Diketahui'];

                if ($request->expectsJson()) {
                    return response()->json($errors, 422);
                }

                $this->incrementLoginAttempts($request);

                return redirect('/login')->withInput($request->only($this->username(), 'remember'))->withErrors($errors);
            }
        }
    }
}
