<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Model\Kategori;
use App\Model\SubKategorie;
use App\Model\KategoriInterior;
use App\Model\Provincie;
use App\Model\Citie;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $link = "";
    private $tableStruct = [];

    public function setLink($value="")
    {
        $this->link = $value;
    }
    public function setTableStruct($value=[])
    {
      $this->tableStruct = $value;
    }

    public function getTableStruct()
    {
      return $this->tableStruct;
    }

    public function render($view,$additional=[])
    {
        $data = [
            'pageUrl'    => $this->link,
            'tableStruct'=> $this->tableStruct,
            'kategori'   => Kategori::get(),
            'subkategori'=> SubKategorie::select('*'),
            'kategoriDesain'=> KategoriInterior::get(),
            'provinsi'   => Provincie::get(),
            'kota'       => Citie::select('*'),
            'mockup'     => true,

        ];
        return view($view, array_merge($data, $additional));
    }
    public function makeButton($params = [])
    {
        $settings = [
            'urls'    => 'others',
            'class'    => 'blue',
            'label'    => 'Button',
            'tooltip'  => '',
            'placement'  => '',
            'target'   => url('/'),
            'disabled' => '',
        ];

        $btn = '';
        $datas = '';
        $attrs = '';

        if (isset($params['datas'])) {
            foreach ($params['datas'] as $k => $v) {
                $datas .= " data-{$k}=\"{$v}\"";
            }
        }

        if (isset($params['attributes'])) {
            foreach ($params['attributes'] as $k => $v) {
                $attrs .= " {$k}=\"{$v}\"";
            }
        }

        switch ($params['type']) {
             case "add-use":
                $settings['class']   = 'green icon delete';
                $settings['label']   = '<i class="users icon"></i>';
                $settings['tooltip'] = 'Add Tenaga Harian';

                $params  = array_merge($settings, $params);
                $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "delete":
                $settings['class']   = 'btn btn-sm btn-danger btn-delete';
                $settings['label']   = '<i class="fas fa-trash"></i>';
                $settings['tooltip'] = 'Hapus Data';
                $settings['placement'] = 'bottom';

                $params  = array_merge($settings, $params);
                $extends = "data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}' data-url='{$params['url']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='btn btn-sm btn-danger {$params['class']}' {$params['disabled']}>{$params['label']}</button>\n";
                break;
            case "edit":
                $settings['class']   = 'btn btn-sm btn-success edit';
                $settings['label']   = '<i class="fa fa-edit"></i>';
                $settings['tooltip'] = 'Ubah Data';
                $settings['placement'] = 'bottom';

                $params  = array_merge($settings, $params);
                $extends = " data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}' data-url='{$params['url']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} button btn-edit' {$params['disabled']}>{$params['label']}</button>\n";
                 break;
            case "approve":
                $settings['class']   = 'btn btn-sm btn-warning approve';
                $settings['label']   = '<i class="fa fa-check"></i>';
                $settings['tooltip'] = 'Approve Data';
                $settings['placement'] = 'bottom';

                $params  = array_merge($settings, $params);
                $extends = " data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                 break;
            case "others":
                $settings['class']   = 'orange icon others';
                $settings['label']   = '<i class="fa fa-plus"></i>';
                $settings['tooltip'] = 'Ubah Data';
                $settings['urls'] = 'others';

                $params  = array_merge($settings, $params);
                $extends = "data-url='{$params['urls']}' data-content='{$params['tooltip']}' data-id='{$params['id']}' ";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                 break;
            case "edit-page":
                $settings['class']   = 'btn btn-sm btn-pink edit';
                $settings['label']   = '<i class="fa fa-edit"></i>';
                $settings['tooltip'] = 'Ubah Data';
                $settings['placement'] = 'bottom';

                $params  = array_merge($settings, $params);
                $extends = " data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                 break;

            case "detail":
                $settings['class']   = 'btn btn-sm btn-warning btn-show';
                $settings['label']   = '<i class="fa fa-eye"></i>';
                $settings['tooltip'] = 'Detail Data';
                $settings['placement'] = 'bottom';

                $params  = array_merge($settings, $params);
                $extends = " data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}' data-url='{$params['url']}'";

                $btn = "<button type='button' {$datas}{$attrs}{$extends} class='{$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
                 break;
            case "detail-page":
              $settings['class']   = 'green icon detail-page';
              $settings['label']   = '<i class="eye icon"></i>';
              $settings['tooltip'] = 'Detail';

              $params  = array_merge($settings, $params);
              $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

              $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";

                break;
          case "view-page":
              $settings['class']   = 'teal icon view-page';
              $settings['label']   = '<i class="eye icon"></i>';
              $settings['tooltip'] = 'Detail';

              $params  = array_merge($settings, $params);
              $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

              $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
              break;
          case "target-blank":
              $settings['class']   = 'green icon';
              $settings['label']   = '<i class="download icon"></i>';
              $settings['tooltip'] = 'Download File';

              $params  = array_merge($settings, $params);
              $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

              $btn = "<a href='{$params['target']}' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']} target='_blank'>{$params['label']}</a>\n";
          break;
          case "modal":
              $settings['class']   = 'orange icon edit';
              $settings['label']   = '<i class="edit icon"></i>';
              $settings['tooltip'] = 'Ubah Data';

              $params  = array_merge($settings, $params);
              $extends = " data-content='{$params['tooltip']}' data-id='{$params['id']}'";

              $btn = "<button type='button' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</button>\n";
            break;
            case "url":
            default:
                $settings['class']   = 'btn btn-sm btn-warning ';
                $settings['label']   = '<i class="fa fa-eye"></i>';
                $settings['tooltip'] = 'Detail Data';

                $params  = array_merge($settings, $params);
                $extends = '';

                $extends = " data-toggle='tooltip' data-placement='{$params['placement']}' title='{$params['tooltip']}' data-id='{$params['id']}'";

                $btn = "<a href='{$params['target']}' {$datas}{$attrs}{$extends} class='ui mini {$params['class']} button' {$params['disabled']}>{$params['label']}</a>\n";
        }

        return $btn;
    }

}
