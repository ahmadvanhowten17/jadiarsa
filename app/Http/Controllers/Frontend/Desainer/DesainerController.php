<?php

namespace App\Http\Controllers\Frontend\Desainer;

use App\Http\Controllers\Controller;
use App\Http\Requests\DesainRequest;
use Illuminate\Support\Facades\Auth;
use App\Model\KategoriInterior;
use App\Model\DesainInterior;
use Illuminate\Http\Request;
use App\Model\Verivication;
use Illuminate\Support\Str;
use App\Model\Lapak;

class DesainerController extends Controller
{
    protected $link = 'jadi-desainer';

    public function __construct()
    {
        $this->setLink($this->link);
    }

    public function index()
    {
        if(Auth::user()){
            $verif = Verivication::where('user_id',Auth::id())->count();
            return $this->render('frontend.pages.desainer.index',[
                'verif' => $verif,
                'lapak'  => Lapak::where('user_id',Auth::id())->first()
            ]);
        }else{
            return redirect('login');
        }
    }

    public function listDesain()
    {
        return $this->render('frontend.pages.desainer.show-list-desain',[
            'record' => DesainInterior::where('created_by',Auth::id())->get()
        ]);
    }

    public function show(Request $request)
    {
        return $this->render('frontend.pages.desainer.show-ajax',[
            'record' => DesainInterior::find($request->id)
        ]);
    }

    public function create(Request $req)
    {
        return $this->render('frontend.pages.desainer.create',[
            'id_lapak' => $req->id
        ]);
    }

    public function edit($id)
    {
        return $this->render('frontend.pages.desainer.edit',[
            'record' => DesainInterior::find($id)
        ]);
    }

    public function store(DesainRequest $request)
    {
        $request->validate([
            'atachment' => 'required',
            'atachment.*' => 'image|mimes:png,jpg,jpeg|max:500',
        ],[
            'atachment.*.max' => 'Gambar Tidak Boleh Lebih Dari 500 kilobite'
        ]);
        $data = $request->all();
        $photo = '';
        try {
            $harga_normal = preg_replace('/[^0-9]/','', $request->harga_normal);
            $harga_interior = preg_replace('/[^0-9]/','', $request->harga_interior);
            $normal = \substr($harga_normal,0,-2);
            $total = \substr($harga_interior,0,-2);
            $data['harga_normal'] = $normal;
            $data['harga_interior'] = $total;
            $data['slug'] = str::slug($request->nama_interior);
            $data['created_by'] = Auth::id();
            $lapak = DesainInterior::create($data);
            if ($request->hasFile('atachment')) {
                $photo = $request->file('atachment');
                foreach($photo as $file){
                    $name   = $file->getClientOriginalName();
                    $url    =$file->store('asset/desain','public');
                    $lapak->attachment()->create([
                        'filename' => $name,
                        'url' => $url,
                    ]);
                }
            }
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'ee'
        ]);

    }

    public function update(DesainRequest $request,$id)
    {
        $request->validate([
            'atachment' => 'nullable',
            'atachment.*' => 'nullable|image|mimes:png,jpg|max:2048',
        ]);
        $data = $request->all();
        $photo = '';
        try {
            $harga_normal = preg_replace('/[^0-9]/','', $request->harga_normal);
            $harga_interior = preg_replace('/[^0-9]/','', $request->harga_interior);
            $normal = \substr($harga_normal,0,-2);
            $total = \substr($harga_interior,0,-2);
            $data['harga_normal'] = $normal;
            $data['harga_interior'] = $total;
            $data['slug'] = str::slug($request->nama_interior);
            $data['updated_by'] = Auth::id();
            $desain = DesainInterior::findOrFail($id);
            $desain->update($data);
            if ($request->hasFile('atachment')) {
                foreach($desain->attachment as $image){
                    $path = public_path().'/storage/'.$image->url;
                    if(\file_exists($path)){
                        unlink($path);
                    }
                }
                $desain->attachment()->delete();
                $photo = $request->file('atachment');
                foreach($photo as $file){
                    // dd($file);
                    $name   = $file->getClientOriginalName();
                    $url    =$file->store('asset/desain','public');
                    $desain->attachment()->create([
                        'filename' => $name,
                        'url' => $url,
                    ]);
                }
            }
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => 'ee'
        ]);
    }

    public function delete($id)
    {
        try{
            $desain = DesainInterior::findOrFail($id);
            $desain->delete();
            foreach($desain->attachment as $image){
                $path = public_path().'/storage/'.$image->url;
                if(\file_exists($path)){
                    unlink($path);
                }
            }
            $desain->attachment()->delete();
        }catch(\Throwable $e){
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }

       return response([
            'status' => true,
            'url'    => $this->link
        ]);
    }
}
