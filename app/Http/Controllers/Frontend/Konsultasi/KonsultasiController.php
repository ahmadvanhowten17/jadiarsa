<?php

namespace App\Http\Controllers\Frontend\Konsultasi;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\User;
class KonsultasiController extends Controller
{
    protected $link = 'konsultasi';
    private $database;
    public $key;
    public function __construct()
    {
        $this->setLink($this->link);
        $this->database = app('firebase.database');
    }

    public function index()
    {
        $picture = Auth::user()->pictures()->first();
        $id = Auth::user()->id;
        $dataUser = [
            'photoURL' => $picture  ? 'storage/'.$picture->url : 'user.png',
            'status' => Auth::user()->status,
            'aktiv' => date('d-m-Y H:i:s'),
            'email' => Auth::user()->email,
            'username' => Auth::user()->username,
        ];
        $flag = false;
        $data = $this->database->getReference('users')->getSnapshot();
        $value = $data->getValue();
        if ($value) {
            foreach ($value as $key => $e) {
                if ($e['email'] === $dataUser['email']) {
                    $this->key = $key;
                    $datachild = $this->database->getReference('users')->getChild(Auth::user()->user_key)->update([
                        'aktiv' => date('d-m-Y H:i:s'),
                        'photoURL' => $picture  ? 'storage/'.$picture->url : 'user.png'
                        ]);
                    $flag = true;
                }
            }
        }
        if ($flag === false) {
            $userkey = $this->database->getReference('users')->push($dataUser);
            User::where('id',$id)->update([
                'user_key' => $userkey->getKey()
            ]);
        }
        return $this->render('frontend.pages.konsultasi.index',[
            'all_chat' => $value
        ]);
    }

    public function userAktiv()
    {
        
        $datachild = $this->database->getReference('users')->getChild(Auth::user()->user_key)->getValue();
        return $this->render('frontend.pages.konsultasi.user',[
            'user'      => $datachild
        ]);
    }
    public function listchat()
    {
        $data = $this->database->getReference('friend_list');
        $val = $data->getValue();
        if ($val !== '') {
            foreach ($val as $key => $isi) {
                $frienKey = '';
                if ($isi['friendId'] === Auth::user()->user_key) {
                    $frienKey = $isi['userId'];
                }
                elseif ($isi['userId'] === Auth::user()->user_key) {
                    $frienKey = $isi['friendId'];
                }
                if ($frienKey !== '') {
                $datakey[] = $this->database->getReference('users')->getChild($frienKey)->getKey();
                $value[] = $this->database->getReference('users')->getChild($frienKey)->getValue();
                }
            }
        }
        return $this->render('frontend.pages.konsultasi.list-chat',[
            'chat'  => $value,
            'datakey'   => $datakey
        ]);
    }
    public function addfriend(Request $request)
    {
        $friendlist = [
            'friendId' => $request->key,
            'userId' => Auth::user()->user_key
        ];
        $chatkey = '';
        $frienphoto = $request->frienimage;
        $data = $this->database->getReference('friend_list');
        $val = $data->getValue();
        $flag = false;
        if ($val) {
            foreach ($val as $key => $value) {
                if (($value['friendId'] === $friendlist['friendId'] && $value['userId'] === $friendlist['userId']) || (($value['friendId'] === $friendlist['userId'] && $value['userId'] === $friendlist['friendId']))) {
                    $chatkey = $data->getChild($key)->getKey();
                    $flag = true;
                }
            }
        }
         if ($flag === false) {
            $data->push($friendlist);
         }
         $data = $this->database->getReference('chatMeseges')->getChild($chatkey);
         $value = $data->getValue();
         $message = '';
         return $this->render('frontend.pages.konsultasi.chat-room',[
             'chatkey' => $chatkey,
             'message' => $value,
             'frienphoto' => $frienphoto
         ]);
    }

    public function startchat(Request $req)
    {
        $chatroom = [
            'userId' => Auth::user()->user_key,
            'msg'   => $req->isi,
            'dateTime' => date('d-m-Y H:i:s')
        ];
        $data = $this->database->getReference('chatMeseges')->getChild($req->datakey);
        $push = $data->push($chatroom);
        $chat = $this->database->getReference('chatMeseges')->getChild($req->datakey);
         $value = $chat->getValue();
         return $this->render('frontend.pages.konsultasi.chat-room',[
            'chatkey' => $req->datakey,
            'message' => $value,
            'frienphoto' => $req->frienimage
        ]);
    }
}
