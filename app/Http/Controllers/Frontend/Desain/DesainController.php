<?php

namespace App\Http\Controllers\Frontend\Desain;

use App\Http\Controllers\Controller;
use App\Http\Requests\DesainRequest;
use Illuminate\Support\Facades\Auth;
use App\Model\KategoriInterior;
use App\Model\DesainInterior;
use Illuminate\Http\Request;
use App\Model\Verivication;
use Illuminate\Support\Str;
use App\Model\Lapak;
use DateTime;
class DesainController extends Controller
{
    protected $link = 'desain';

    public function __construct()
    {
        $this->setLink($this->link);
    }

    public function index()
    {
        return $this->render('frontend.pages.desain.index',[
            'record' => KategoriInterior::with('attachment')->get(),
        ]);
    }

    public function show($slug)
    {
        $record = DesainInterior::with('attachment','lapak.user.pictures')->whereHas('kategoridesain',function($e) use($slug){
            $e->where('slug',$slug);
        })->get();
        return $this->render('frontend.pages.desain.show',[
            'record' => $record
        ]);
    }

    public function listdetail($slug)
    {
        $record = DesainInterior::with('lapak.user.pictures')->where('slug',$slug)->first();
        $estimasi = \str_replace('-',' ',$record->estimasi);
        $dateone = \substr($estimasi,3,2);
        $datetwo = \substr($estimasi,15,3);
        $bulan = $datetwo - $dateone;
        $pengerjaan = $bulan;
        return $this->render('frontend.pages.desain.show-desain',[
            'record' => $record,
            'pengerjaan' => $pengerjaan
        ]);
    }

}
