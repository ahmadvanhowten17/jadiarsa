<?php

namespace App\Http\Controllers\Frontend\Searching;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Lapak;
use App\Model\LapakBarang;
class SearchingController extends Controller
{
    protected $link = 'search';
    public function __construct()
    {
        $this->setLink($this->link);
    }

    public function searchToko(Request $request)
    {
        return $this->render('frontend.pages.searching.index',[
            'request' => $request
        ]);
    }

    public function renderData(Request $request)
    {
        $slug = $request->slug;
        $data = str_replace('-',' ',$slug);
        $record = LapakBarang::whereHas('lapaks',function($e) use($data){
            $e->where('type_toko','like', '%'.$data.'%');
        })->select('*');
        $record = $record->paginate(25);
        return $this->render('frontend.pages.searching.ajax.index',[
            'record' => $record,
        ]);
    }

    public function filterData(Request $request)
    {
        $provincie_id = $request->provincie_id;
        $city_id = $request->city_id;
        $harga = $request->harga;
        if ($provincie_id !='') {
            $record = LapakBarang::whereHas('lapaks',function($e) use($provincie_id){
                $e->where('provincie_id','like', '%'.$provincie_id.'%');
            })->select('*');
        }
        elseif ($city_id !=''){
            $record = LapakBarang::whereHas('lapaks',function($e) use($city_id){
                $e->where('citie_id','like', '%'.$city_id.'%');
            })->select('*');
        }
        elseif ($harga !=''){
            $record = LapakBarang::where('harga_barang','like', '%'.$harga.'%')->select('*');
        }
        $record = $record->paginate(25);
        return $this->render('frontend.pages.searching.ajax.index',[
            'record' => $record,
        ]);
    }
}
