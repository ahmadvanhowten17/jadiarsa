<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use App\Model\Provincie;
use App\Model\Citie;
use App\Model\LapakBarang;

class HomeController extends Controller
{
    public function index()
    {
        $record = LapakBarang::orderBy('created_at','desc')->get();
        return $this->render('frontend.pages.home',[
            'active' => 'home',
            'record' => $record,
        ]);
    }

    public function show($slug)
    {
        $apiKey = Config('services.rajaongkir.key');
        $apiUrl = Config('services.rajaongkir.url');
        $record = LapakBarang::with('lapaks.cities')->where('slug',$slug)->orderBy('created_at','desc')->first();
        $cek = Http::withHeaders([
            // 'content-type' => 'application/x-www-form-urlencoded',
            'key' => $apiKey,
        ])->post($apiUrl, [
            'origin' => $record->lapaks->citie_id ?? 23,
            'destination' => 23,
            'weight' => $record->berat_barang,
            'courier'=> 'jne'
        ])->body();
        $cek = \json_decode($cek,true);
        foreach ($cek['rajaongkir']['results'] as $key) {
            $ongkir = $key['costs'][0]['cost'];
        }
        return $this->render('frontend.pages.home.show-barang',[
            'record' => $record,
            'bredcrumb' => $slug,
            'ongkir'    => $ongkir
        ]);
    }

    public function barangList(Request $req)
    {
        $id = $req->id;
        if($id){
            $record = LapakBarang::whereHas('kategories',function($e) use($id){
                $e->where('id',$id);
            })->get();
        }
        return $this->render('frontend.pages.list-product',[
            'record' => $record,
            'id'     => $req->idBarang
        ]);
    }

    public function listKota(Request $req)
    {
        $id = $req->id;
        $apiKey = Config('services.rajaongkir.key');
        $apiUrl = Config('services.rajaongkir.url');
        if ($id) {
            $cek = Http::withHeaders([
                // 'content-type' => 'application/x-www-form-urlencoded',
                'key' => $apiKey,
            ])->post($apiUrl, [
                'origin' => $id,
                'destination' => $req->text,
                'weight' => $req->weight,
                'courier'=> 'jne'
            ])->body();
            $cek = \json_decode($cek,true);
            foreach ($cek['rajaongkir']['results'] as $key) {
                $ongkir = $key['costs'][0]['cost'];
            }
        }
        return $this->render('frontend.pages.home.ajax-ongkir',[
            'ongkir' => $ongkir,
        ]);
    }
    public function works()
    {
        return view('frontend.pages.works',[
            'active' => 'work'
        ]);
    }

    public function shop()
    {
        return view('frontend.pages.shop',[
            'active' => 'shop'
        ]);
    }

    public function join()
    {
        return view('frontend.pages.join-us',[
            'active' => 'join'
        ]);
    }

    public function about()
    {
        return view('frontend.pages.about',[
            'active' => 'about'
        ]);
    }

    public function contact()
    {
        return view('frontend.pages.contact',[
            'active' => 'contact'
        ]);
    }
}
