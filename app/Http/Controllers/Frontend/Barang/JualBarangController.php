<?php

namespace App\Http\Controllers\Frontend\Barang;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BarangRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\LapakBarang;
use App\Model\Lapak;

class JualBarangController extends Controller
{
    protected $link = 'jual-barang';

    public function __construct()
    {
        $this->setLink($this->link);
    }

    public function index()
    {
        if (auth()->user()->status_veriv != null) {
            # code...
            return $this->render('frontend.pages.barang.index',[
                'lapak' => Lapak::where('user_id',Auth::id())->first()
            ]);
        }else {
            return \abort(404);
        }
    }

    public function show($id)
    {
        $record = LapakBarang::findOrFail($id);
        if ($record->created_by == Auth::id()) {
            return $this->render('frontend.pages.barang.show',[
                'record' => $record
            ]);
        }else {
            return \abort(404);
        }
    }

    public function edit($id)
    {
        $record = LapakBarang::findOrFail($id);
        if ($record->created_by == Auth::id()) {
            return $this->render('frontend.pages.barang.edit',[
                'record' => $record
            ]);
        }else {
            return \abort(404);
        }
    }
    public function store(BarangRequest $request)
    {
        $request->validate([
            'atachment' => 'required',
            'atachment.*' => 'image|mimes:png,jpg|max:2048',
        ]);
        $data = $request->all();
        $photo = '';
        try {
            $harga_normal = preg_replace('/[^0-9]/','', $request->harga_normal);
            $harga_barang = preg_replace('/[^0-9]/','', $request->harga_barang);
            $normal = \substr($harga_normal,0,-2);
            $total = \substr($harga_barang,0,-2);
            $data['harga_normal'] = $normal;
            $data['harga_barang'] = $total;
            $data['slug'] = str::slug($request->nama_barang);
            $data['created_by'] = Auth::id();
            $lapak = LapakBarang::create($data);
            if ($request->hasFile('atachment')) {
                $photo = $request->file('atachment');
                foreach($photo as $file){
                    $name   = $file->getClientOriginalName();
                    $url    =$file->store('asset/barang','public');
                    $lapak->attachment()->create([
                        'filename' => $name,
                        'url' => $url,
                    ]);
                }
            }
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => $this->link
        ]);

    }
    public function update(BarangRequest $request, $id)
    {
        $request->validate([
            'atachment' => 'nullable',
            'atachment.*' => 'nullable|image|mimes:png,jpg|max:2048',
        ]);
        $data = $request->all();
        $photo = '';
        try {
            $harga_normal = preg_replace('/[^0-9]/','', $request->harga_normal);
            $harga_barang = preg_replace('/[^0-9]/','', $request->harga_barang);
            $normal = \substr($harga_normal,0,-2);
            $total = \substr($harga_barang,0,-2);
            $data['harga_normal'] = $normal;
            $data['harga_barang'] = $total;
            $data['slug'] = str::slug($request->nama_barang);
            $data['updated_by'] = Auth::id();
            $lapak = LapakBarang::findOrFail($id);
            $lapak->update($data);
            if ($request->hasFile('atachment')) {
                foreach($lapak->attachment as $image){
                    $path = public_path().'/storage/'.$image->url;
                    if(\file_exists($path)){
                        unlink($path);
                    }
                }
                $lapak->attachment()->delete();
                $photo = $request->file('atachment');
                foreach($photo as $file){
                    // dd($file);
                    $name   = $file->getClientOriginalName();
                    $url    =$file->store('asset/barang','public');
                    $lapak->attachment()->create([
                        'filename' => $name,
                        'url' => $url,
                    ]);
                }
            }
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => $this->link
        ]);
    }
    public function delete($id)
    {
        try{
            $barang = LapakBarang::findOrFail($id);
            $barang->delete();
            foreach($barang->attachment as $image){
                $path = public_path().'/storage/'.$image->url;
                if(\file_exists($path)){
                    unlink($path);
                }
            }
            $barang->attachment()->delete();
        }catch(\Throwable $e){
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }

       return response([
        'status' => true,
        'url'    => $this->link
    ]);
    }
}
