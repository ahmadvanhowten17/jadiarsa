<?php

namespace App\Http\Controllers\Frontend\Kategori;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SubKategorie;
use App\Model\ChildKategorie;

class KategoriController extends Controller
{
    protected $link = 'kategori';

    public function __construct()
    {
        $this->setLink($this->link);
    }

    public function subkategori(Request $request)
    {
        $id = $request->id;
        if($id){
            $record = SubKategorie::where('kategori_id',$id)->get();
        }
        return $this->render('frontend.pages.kategori.kategori',[
            'record' => $record
        ]);
    }

    public function chilKategori(Request $request)
    {
        $id = $request->id;
        if($id){
            $record = ChildKategorie::where('sub_kategorie_id',$id)->get();
        }
        return $this->render('frontend.pages.kategori.kategori',[
            'record' => $record
        ]);
    }
}
