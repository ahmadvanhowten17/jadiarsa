<?php

namespace App\Http\Controllers\Frontend\Toko;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\VerivToko;
use App\Model\Lapak;
use App\Model\LapakBarang;
use App\User;
use App\Model\Verivication;

class TokoController extends Controller
{
    protected $link = 'jadi-punya-toko';
    public function __construct()
    {
        $this->setLink($this->link);
    }
    public function index()
    {
        $record = Lapak::where('user_id',Auth::id())->first();
        $verif = Verivication::where('user_id',Auth::id())->count();
        return $this->render('frontend.pages.toko.index',[
            'record' => $record,
            'verif' => $verif
        ]);
    }

    public function showProduct(Request $request)
    {
        return $this->render('frontend.pages.toko.show-product',[
            'record' => LapakBarang::where('created_by',Auth::id())->orderBy('created_at','desc')->get()
        ]);
    }

    public function create()
    {
        return $this->render('frontend.pages.toko.create');
    }
    public function edit ($id)
    {
        return $this->render('frontend.pages.toko.edit',[
            'record' => Lapak::find($id)
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama_toko' => 'required',
            'deskripsi_toko' => 'required',
            'alamat_toko' => 'required',
            'kode_pos' => 'required|integer',
            'phone' => 'required|integer',
        ]);

        try {
            $data = $request->all();
            $data['user_id'] = Auth::id();
            $data['created_by'] = Auth::id();
            $data = Lapak::create($data);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => $this->link
        ]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_toko' => 'required',
            'deskripsi_toko' => 'required',
            'alamat_toko' => 'required',
            'kode_pos' => 'required|integer',
            'phone' => 'required|integer',
        ]);
        try {
            $data = $request->all();
            $data['updated_by'] = Auth::id();
            $lapak = Lapak::findOrFail($id);
            $lapak->update($data);
        } catch (\Throwable $e) {
            return response([
                'status' => 'error',
                'message' => $e,
            ],500);
        }
        return response([
            'status' => true,
            'url'    => $this->link
        ]);
    }
    public function veriv(Request $request)
    {
        $fordigit = rand(1000,9999);
        $verif = Verivication::create([
            'user_id' => Auth::id(),
            'kode_veriv' => $fordigit
        ]);
        Mail::to(Auth::user()->email)->send(new VerivToko($verif));
        return response([
            'status' => true,
            'url'    => $this->link
        ]);
    }

    public function cekKode(Request $request)
    {
        $verif = new Verivication;
        $id = $request->id;
        $kode = $request->kode;

        if ($verif->where('user_id',$id)->where('kode_veriv',$kode)->first()) {
                $user = User::where('id',$id)->update([
                    'status_veriv' => 'verived'
                ]);
                $data = Verivication::where('user_id',$id)->first();
                $data->delete();
            return response([
                'status' => true,
                'url'    => $this->link
            ]);
        }else{
            return response([
                    'status' => 'error',
                    'message' => 'Ops Data Yang Anda Masukan Tidak Ditemukan',
                ],500);
        }
    }
}
