<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Model\PictureUser;
use App\Model\Provincie;
use App\Model\Citie;
use App\User;
use Auth;
class MyprofileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $record = User::where('id',$id)->first();
        $provinsi = Provincie::get();
        $kota = Citie::get();
        return $this->render('frontend.pages.profile.index',[
            'record' => $record,
            'active' => false,
            'provinsi' => $provinsi,
            'kota' => $kota,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        if ($id) {
            $record = Citie::where('provin_id', $id)->get();
        }
        return $this->render('frontend.pages.profile.show',[
            'record' => $record
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, $id)
    {
        $photo = '';
        if ($request->hasFile('url')) {
            $photo = $request->file('url')->store('asset/profile','public');
            $name = $request->file('url')->getClientOriginalName();
        }

        $data = User::find($id);
        $profile = $data->update([
            'username' => $request->username,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'gender' => $request->gender,
            'provin_id' => $request->provin_id,
            'city_id' => $request->city_id,
        ]);
        $data->pictures()->create([
            'name' => $name,
            'url' => $photo
        ]);

        return redirect('myprofile')->with(['success' => 'data Profile berhasil di update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
