<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->status === 'admin') {
                return $next($request);
            }elseif (Auth::user()->status === 'superadmin') {
                return $next($request);
            }
        }
        return abort('403');
    }
}
