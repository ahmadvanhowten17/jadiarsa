<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Model\Verivication;

class VerivToko extends Mailable
{
    use Queueable, SerializesModels;

    public $verif;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($verif)
    {
        $this->verif = $verif;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.verivikasi.toko');
    }
}
