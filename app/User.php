<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Model\PictureUser;
use App\Model\Citie;
use App\Model\Kategori;
use App\Model\Verivication;
use App\Model\Lapak;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'status',
        'alamat',
        'gender',
        'provider',
        'provider_id',
        'provin_id',
        'city_id',
        'status_veriv'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pictures()
    {
        return $this->hasMany(PictureUser::class);

    }

    public function Verivications()
    {
        return $this->hasMany(Verivication::class);

    }

    public function Cities()
    {
        return $this->belongsTo(Citie::class,'city_id');
    }

    public function lapaks()
    {
        return $this->hasMany(Lapak::class);
    }
}
