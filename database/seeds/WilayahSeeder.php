<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Model\Provincie;
use App\Model\Citie;
class WilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('provincies')->truncate();
        // DB::table('cities')->truncate();
        $provinsi = Http::get('https://api.rajaongkir.com/starter/province',[
            'key' => '840414d7e738d52abd90045d1f47524b'
        ])->body();
        $provinsi = \json_decode($provinsi,true);

        $kota = Http::get('https://api.rajaongkir.com/starter/city',[
            'key' => '840414d7e738d52abd90045d1f47524b'
        ])->body();
        $kota = \json_decode($kota,true);

        foreach ($provinsi['rajaongkir']['results'] as $key) {
            $d = Provincie::create([
                'id' => $key['province_id'],
                'provinsi' => $key['province'],
            ]);

        }
        foreach ($kota['rajaongkir']['results'] as $key) {
            $s = Citie::create([
                'id' => $key['city_id'],
                'provin_id' => $key['province_id'],
                'kota' => $key['city_name'],
                'type' => $key['type'],
            ]);

        }
    }
}
