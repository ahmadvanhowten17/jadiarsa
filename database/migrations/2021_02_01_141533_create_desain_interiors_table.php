<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesainInteriorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desain_interiors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lapak_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->string('nama_interior')->nullable();
            $table->string('slug')->nullable();
            $table->longText('deskripsi_interior')->nullable();
            $table->string('jumlah_ruangan')->nullable();
            $table->string('disc_interior')->nullable();
            $table->string('type_interior')->nullable();
            $table->string('jumlah_lantai')->nullable();
            $table->bigInteger('harga_normal')->nullable();
            $table->bigInteger('harga_interior')->nullable();
            $table->string('estimasi')->nullable();
            $table->foreignId('kategori_interior_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('provincie_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('citie_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desain_interiors');
    }
}
