<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildKategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_kategories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sub_kategorie_id')->nullable()
                        ->constrained()
                        ->onUpdate('cascade')
                        ->onDelete('cascade');
            $table->string('nama')->nullable();
            $table->string('slug')->nullable();

            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_kategories');
    }
}
