<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLapakBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapak_barangs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('lapak_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->string('nama_barang')->nullable();
            $table->string('slug')->nullable();
            $table->longText('deskripsi_barang')->nullable();
            $table->bigInteger('harga_normal')->nullable();
            $table->string('berat_barang')->nullable();
            $table->string('disc_barang')->nullable();
            $table->bigInteger('harga_barang')->nullable();
            $table->integer('stock_barang')->nullable();
            $table->integer('barang_terjual')->nullable();
            $table->integer('minimum_pembelian')->nullable();
            $table->string('kondisi_barang')->nullable();
            $table->string('merek')->nullable();
            $table->string('status_barang')->nullable();
            $table->foreignId('kategori_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('sub_kategorie_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('child_kategorie_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->nullable();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lapak_barangs');
    }
}
