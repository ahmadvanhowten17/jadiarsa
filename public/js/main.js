// frontend
var mybutton = document.getElementById("mybuton");

    window.onscroll = function() {
      scrollFunction()
    };
function scrollFunction() {
    if(mybutton != null){
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
    }
}
function topFunction() {
document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
}
function convertToMata(angka){
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
		var hasil = ''+rupiah.split('',rupiah.length-1).reverse().join('');
	if(hasil == 'NaN'){
		hasil = '';
	}else{
		hasil = hasil+',00';
	}
	return 'Rp.'+hasil;
}
$(document).on('click', '.dropdown-menu', function (e) {
    e.stopPropagation();
  });

  // make it as accordion for smaller screens
  if ($(window).width() < 992) {
        $('.dropdown-menu a').on('click',function(e){
            e.preventDefault();
          if($(this).next('.submenu').length){
              $(this).next('.submenu').toggle();
          }
          $('.dropdown').on('hide.bs.dropdown', function () {
             $(this).find('.submenu').hide();
          })
        });
  }
$(document).on('click','.dropdowns-toggle', function(){
    $(".dropdowns-menu").toggle();
});

//   owl
$('.loop').owlCarousel({
    center: true,
    items: 1,
    margin:10,
    loop:true,
    nav: true,
    navText: ["<div class='nav-button owl-prev'>‹</div>", "<div class='nav-button owl-next'>›</div>"],
    autoplay:true,
    autoplayTimeout:5000,
    responsive:{
        600:{
            items:1
        }
    }
});
$('.loop-product').owlCarousel({
    loop:true,
    margin:10,
    dots: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            loop:false
        },
        600:{
            items:3,
            nav:false,
            loop:false
        },
        1000:{
            items:4,
            loop:false
        }
    }
});

$(document).on('click','.btn-veriv',function(){
    var _token = $('meta[name="csrf-token"]').attr('content');
    var url = $(this).data('url');
    $.ajax({
        type: "POST",
        url: url + '/veriv',
        data:{
            _token:_token,
        },
        success: function(resp){
            swal(
                'Tersimpan!',
                'Verivikasi Succes Cek Email Anda .',
                'success'
                ).then((result) => {
                    if(resp.url){
                        window.location = resp.url;
                    }
                    dt.draw();
                })
            },
            error: function(resp){
                swal(
                'Gagal Menyimpan Data!',
                'Oops Terjadi Kesalahan' ,
                'error'
                );
                // showFormErrorModalTwo(resp,form);
        }
    });
});

$(document).on('click','.btn-kode',function(){
    var kode = $('#inlineFormInputGroup').val();
    var _token = $('meta[name="csrf-token"]').attr('content');
    var url = $(this).data('url');
    var id = $(this).data('id');
    $.ajax({
        type: "POST",
        url: url + '/veriv/' + id,
        data:{
            _token:_token,
            _method: "put",
            id: id,
            kode: kode,
        },
        success: function(resp){
            swal(
                'Tersimpan!',
                'Berhasil Verivikasi Email .',
                'success'
                ).then((result) => {
                    if(resp.url){
                        window.location = resp.url;
                    }
                    dt.draw();
                })
            },
            error: function(resp){
                swal(
                'Gagal Menyimpan Data!',
                "Ops Data Yang Anda Masukan Tidak Ditemukan",
                'error'
                );
                // showFormErrorModalTwo(resp,form);
        }
    });
});

$(document).on('change','.data-provin',function(e){
    e.preventDefault();
    var id = $(this).val();
    var url = $(this).data('url');
    var _token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id:id,
            _token:_token
        },
        success: function(resp){
            $('#list-kota').html(resp);
        }
    });
});
// kategori
$(document).on('click','.data-kat',function(){
    var id = $(this).val();
    var url = $(this).data('url');
    var _token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id:id,
            _token:_token
        },
        success: function(resp){
            $('.data-sub').html(resp);
        }
    });
});
$(document).on('click','.data-sub',function(){
    var id = $(this).val();
    var url = $(this).data('url');
    var _token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        type: "POST",
        url: url,
        data: {
            id:id,
            _token:_token
        },
        success: function(resp){
            $('.data-child').html(resp);
        }
    });
});
// tutup kategori
function listProduct(url, id='',idBarang='')
{
    $.ajax({
        type: "GET",
        url: url,
        data: {id:id,idBarang:idBarang},
        success: function(resp){
            $('.list-product').html(resp);
            $('.slide').owlCarousel({
                loop:true,
                margin:10,
                dots: false,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2,
                        loop:false
                    },
                    600:{
                        items:3,
                        nav:false,
                        loop:false
                    },
                    1000:{
                        items:4,
                        loop:false
                    }
                }
            });
        }
    });
}
// numerik
function numerik()
{
    const anElement = AutoNumeric.multiple('.change-money', {
        'digitGroupSeparator': '.',
        'decimalPlaces': '2',
        'decimalCharacter': ',',
        'currencySymbol': 'Rp.',
    });
}

function numeriks()
{
    const anElement = AutoNumeric.multiple('#format', {
        'digitGroupSeparator': '.',
        'decimalPlaces': '2',
        'decimalCharacter': ',',
        'currencySymbol': 'Rp.',
    });
}

// disc
$(document).on('keyup','.change-money',function(){
	var value = $(this).val();
	$('.sub-total').val(value+',00');
});

$(document).on('keyup','.disc',function() {
	var isi = $(this).val();
	var val = $('.change-money').val();
	var pars = val.replace(/\D/g,'');
	var int = pars.substring(0, pars.length-2);

	var disc = isi * int /100;
	var hasil = int - disc ;
	var convert = convertToMata(hasil);
	$('.sub-total').val(convert);
});

// lokasi barang
$(document).on('click','.ubah-alamat',function(){
    $('.lokasi-ongkir').toggle();
    $('#input-lokasi').select2();
});

$(document).on('change','#input-lokasi',function(){
    var text = $(this).val();
    var url = $(this).data('url');
    var id = $(this).data('id');
    var weight = $(this).data('weight');
    var _token = $('meta[name="csrf-token"]').attr('content');
    var str = "";
    $( "#input-lokasi option:selected" ).each(function() {
        str += $( this ).text() + " ";
    });
    $('.lokasi-ongkir').hide();
    $.ajax({
        type: "POST",
        url: url + '/cek-ongkir',
        data:{
            _token:_token,
            id:id,
            weight:weight,
            text:text
        },
        success: function(resp){
            $('.kota').html(str);
            $('.estimasi').html(resp);
        }
    });
});

$(document).on('click','.show-img',function(){
   var loc = $(this).attr('src');
   $('.product').attr('src',loc);
});
function fileInput()
{
    $("#input-700").fileinput({
        maxFileCount : 5,
        allowedFileTypes: ['image'],
        showCancel: true,
        initialPreviewAsData: true,
        overwriteInitial: false,
        theme: 'fas',
    });
}
function datetime()
{
    $('#reservation').daterangepicker({
        "locale": {

            "format": "DD-MM-YYYY",
            "separator": " / ",
            "applyLabel": "Tambah",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",

            "firstDay": 1
        },
        "weekStart": 5,
        "showWeekNumbers": true,
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
        format: 'MM/DD/YYYY hh:mm A'
        }
    });
}

// chat
function konsultasi(url)
{
    $.ajax({
        type: "GET",
        url: url + '/user-aktiv',
        success: function(resp){
            $('.chat-title').html(resp);
        }
    });
}
function listchat(url)
{
    $.ajax({
        type: "GET",
        url: url + '/list-chat',
        success: function(resp){
            $('#listChat').html(resp);
        }
    });
}
$(document).on('click','.btn-friend',function(){
    $('.friend-load').removeClass('d-none');
    $('#divstart').hide();
    $('.chat-title').addClass('chat-mobile');
    friendkey = $(this).data('key');
    let _token = $('meta[name="csrf-token"]').attr('content');
    let url = $(this).data('url');
    let key = $(this).data('key');
    let frienimage = $('.img-friend').attr('src');
    let friendName = $(this).data('name');
    addchat(url,_token,key,frienimage,friendName);
    setInterval(function () {
        $('#message').load(url,{
            _token,
            key,
            frienimage,
            friendName
        });
    }, 10000)
});
function addchat(url,_token,key='',frienimage='',friendName='')
{
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            key:key,
            frienimage:frienimage,
            _token:_token,
        },
        success: function(resp)
        {
            $('#message').html(resp);
            $('#chatpanel').show();
            $('#divChatName').html(friendName);
            $('#imgChat').attr('src',frienimage);
            document.getElementById('message').scrollTo(0, document.getElementById('message').clientHeight);
            $('.friend-load').addClass('d-none');
        }
    });
}
$(document).on('keypress','#txmessage',function(event){
    let keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        let isi = $(this).val();
        let _token = $('meta[name="csrf-token"]').attr('content');
        let datakey = $('.key').data('key');
        let frienimage = $('#imgChat').attr('src');
        let url = $(this).data('url');
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                datakey:datakey,
                friendkey:friendkey,
                frienimage:frienimage,
                isi:isi,
                _token:_token
            },
            success: function(resp){
                $('#message').html(resp);
                $('#txmessage').val('');
                $('#txmessage').focus();
                document.getElementById('message').scrollTo(0, document.getElementById('message').clientHeight);
            }
        });
    }

});
// end chat


//   backend admin
function selectTwo()
{
    $('.select2').select2();
}
function loadmodal(url, id='', title=''){
    $.ajax({
        type: "GET",
        url: url,
        data:{
            id:id,
            title:title
        },
        success: function(resp){
            $('.form-modal').html(resp);
            $('.modal-title').html(title);
            $('.modal').modal('show');
            $('.summernote').summernote({
                placeholder: 'Silahkan Masukan Deskripsi Barang..',
                tabsize: 2,
                height: 120,
                toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ]
            });
            numerik();
            selectTwo();
            datetime();
        },
        error: function(resp){
        $('.body-isi').html('data tidak d temukan');
        }
    });
}
function upperCase(data){
    var result = data.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    return letter.toUpperCase();
    });
    return result;
}
function showBoxValidation(resp){
    var temp = ``;
    if(resp.statusText = 'Unprocessable Entity'){
        temp += `<div class="sidebar-widget wow fadeInUp outer-top-vs animated" style="visibility: visible; animation-name: fadeInUp;"><h3 class="section-title text-warning">Informasi</h3><div class="sidebar-widget-body" ><div class="compare-report"><ul class="list text-left bold" style="font-size:16px;list-style:inside;">`;
            if(resp.responseJSON){
                if(resp.responseJSON.errors){
                    var data = resp.responseJSON.errors;
                    $.each(data,function(key,value){
                        temp += `<li><small>`+upperCase(key.replace("_", " "))+` : ` +value[0]+ `</small></li>`;
                    });
                }
            }
        temp += `</ul></div></div></div>`;
    }else{
        temp = 'Terjadi Kesalahan Sistem';
    }
    console.log('temp',temp);
    return temp;
}
function showFormErrorModalTwo(resp, formid){
    var response = resp.responseJSON;
    var addErr = {};

    $.each(response.errors, function (index, val) {
        var response = resp.responseJSON;
        if (index.includes(".")) {
            res = index.split('.');
            index = '';
            for (i = 0; i < res.length; i++) {
                if (i == 0) {
                    res[i] = res[i];
                } else {
                    if (res[i] == 0) {
                        res[i] = '[0]';
                    } else {
                        res[i] = '[' + res[i] + ']';
                    }
                }
                index += res[i];
            }
        }
        clearFormErrorModalTwo(index,val,formid);
        var name = index.split('.').reduce((all, item) => {
            all += (index == 0 ? item : '[' + item + ']');
            return all;
        });

        console.log('index',index)
        console.log('name',name)
        var fg = $('[name="' + name + '"], [name="' + name + '[]"]');
        fg.addClass('has-error');

        fg.after('<small class="control-label error-label font-bold" style="margin-top: 0.25rem;font-size: smaller;color: #ea5455;">' + val + '</small>')
    });
    $("html, body").animate({ scrollTop: 0 }, "slow");
    var intrv = setInterval(function(){
        $('.error-label').slideUp(500, function(e) {
            $(this).remove();
            $('.has-error').removeClass('has-error');
            clearTimeout(intrv);
        });
    }, 14000)
}
function saveFormModal(form,url)
{
    $(form).ajaxSubmit({
        success: function(resp){
            $(".modal").modal('hide');
            swal(
                'Tersimpan!',
                'Data Berhasil Di Simpan.',
                'success'
                ).then((result) => {
                    if(resp.url){
                        window.location = url;
                    }
                    dt.draw();
                })
            },
            error: function(resp){
                swal(
                'Gagal Menyimpan Data!',
                showBoxValidation(resp),
                'error'
                );
                // showFormErrorModalTwo(resp,form);
            }
    });
}
function deletform(urls,_token,url=''){
    swal({
        title: 'Apa Anda Yakin?',
        text: "Data Yang Di Hapus Tidak Dapat Di Kembalikan!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Delete',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result) {
          $.ajax({
            type: 'POST',
            url: urls,
            data: {_token: _token, _method: "delete"},
            success: function(resp){
              swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              ).then(function(e){
                if(resp){
                  window.location = url;
                }
              });
            },
            error: function(resp){
              swal(
                'Gagal!!',
                'Data gagal dihapus, karena sedang dipakai',
                'error'
              ).then(function(e){
                if(resp){
                  window.location = url;
                }
              });
            }
          });
        }
      });
}
$(document).on('click','.add-data',function(){
    var url = $(this).data('url');
    var title = $(this).data('title');
    var id = '';
    loadmodal(url,id,title);
});
$(document).on('click','.add-desain',function(){
    let url = $(this).data('url');
    let title = $(this).data('title');
    let id = $(this).data('id');
    loadmodal(url,id,title);
});
$(document).on('click','.btn-show',function(){
    var url = $(this).data('url');
    var title = $(this).data('title');
    var id = $(this).data('id');
    loadmodal(url,id,title);
});
$(document).on('click','.btn-edit',function(){
    var url = $(this).data('url');
    var title = $(this).data('title');
    var id = $(this).data('id');
    loadmodal(url,id,title);
});
$(document).on('click','.btn-simpan',function(){
    var form = '#dataFormModal';
    var url = $(this).data('url');
    saveFormModal(form,url);
});
$(document).on('click','.btn-delete',function(){
    var _token = $('meta[name="csrf-token"]').attr('content');
    var urls = $(this).data('url');

    deletform(urls,_token);
});







