<?php
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */


    Route::middleware('admin')->namespace('Backend')->group(function () {
        Route::get('/','AdminController@index');
        Route::resource('/user', 'AdminUserController');
        Route::post('/user/data', 'AdminUserController@showData');
    });
    Route::middleware('admin')->namespace('Backend\Kategori')->group(function () {
        Route::get('/kategori', 'KategoriController@index');
        Route::post('/kategori/data', 'KategoriController@showData');
        Route::get('/kategori/create', 'KategoriController@create');
        Route::post('/kategori', 'KategoriController@store');
        Route::get('/kategori/{id}', 'KategoriController@show');
        Route::get('/kategori/{id}/edit', 'KategoriController@edit');
        Route::put('/kategori/{id}', 'KategoriController@update');
        Route::delete('/kategori/{id}','KategoriController@delete');
    });
    Route::middleware('admin')->namespace('Backend\SubKategori')->group(function () {
        Route::get('/subkategori','SubKategoriController@index');
        Route::post('/subkategori/data', 'SubKategoriController@showData');
        Route::get('/subkategori/create','SubKategoriController@create');
        Route::get('/subkategori/{id}','SubKategoriController@show');
        Route::get('/subkategori/{id}/edit','SubKategoriController@edit');
        Route::post('/subkategori','SubKategoriController@store');
        Route::put('/subkategori/{id}','SubKategoriController@update');
        Route::delete('/subkategori/{id}','SubKategoriController@delete');
    });
    Route::middleware('admin')->namespace('Backend\ChildKategori')->group(function () {
        Route::get('/childkategori','ChildKategoriController@index');
        Route::post('/childkategori/data', 'ChildKategoriController@showData');
        Route::get('/childkategori/create','ChildKategoriController@create');
        Route::get('/childkategori/{id}','ChildKategoriController@show');
        Route::get('/childkategori/{id}/edit','ChildKategoriController@edit');
        Route::post('/childkategori','ChildKategoriController@store');
        Route::put('/childkategori/{id}','ChildKategoriController@update');
        Route::delete('/childkategori/{id}','ChildKategoriController@delete');
    });
    Route::middleware('admin')->namespace('Backend\KategoriDesain')->group(function () {
        Route::get('/kategori-desain','KategoriDesainController@index');
        Route::post('/kategori-desain/data', 'KategoriDesainController@showData');
        Route::get('/kategori-desain/create','KategoriDesainController@create');
        Route::get('/kategori-desain/{id}','KategoriDesainController@show');
        Route::get('/kategori-desain/{id}/edit','KategoriDesainController@edit');
        Route::post('/kategori-desain','KategoriDesainController@store');
        Route::put('/kategori-desain/{id}','KategoriDesainController@update');
        Route::delete('/kategori-desain/{id}','KategoriDesainController@delete');
    });
?>
