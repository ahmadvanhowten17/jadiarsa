<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
// socialite
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('register/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('register/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
Route::get('/home', 'HomeController@index')->name('home');
Route::namespace('Frontend')->group(function () {
    Route::get('/','HomeController@index');
    Route::get('/p/{slug}','HomeController@show');
    Route::get('/list/barang-terkait','HomeController@barangList');
    Route::post('/cek-ongkir','HomeController@listKota');
    Route::get('/works','HomeController@works');
    Route::get('/shop','HomeController@shop');
    Route::get('/join-us','HomeController@join');
    Route::get('/about','HomeController@about');
    Route::get('/contact','HomeController@contact');
});
Route::namespace('Frontend\Desain')->group(function () {
    Route::get('/desain','DesainController@index');
    Route::get('/desain/d/{slug}','DesainController@show');
    Route::get('/portfolio/{slug}','DesainController@listdetail');
});
Route::namespace('Frontend\Desainer')->group(function () {
    Route::get('/jadi-desainer','DesainerController@index');
    Route::get('/jadi-desainer/create','DesainerController@create');
    Route::get('/jadi-desainer/show','DesainerController@show');
    Route::get('/jadi-desainer/list-desain','DesainerController@listDesain');
    Route::post('/jadi-desainer','DesainerController@store');
    Route::get('/jadi-desainer/{id}/edit','DesainerController@edit');
    Route::put('/jadi-desainer/{id}','DesainerController@update');
    Route::delete('/jadi-desainer/{id}','DesainerController@delete');
});
Route::middleware('auth')->namespace('Frontend\Toko')->group(function () {
    Route::get('/jadi-punya-toko','TokoController@index');
    Route::get('/jadi-punya-toko/create','TokoController@create');
    Route::get('/jadi-punya-toko/show-product','TokoController@showProduct');
    Route::get('/jadi-punya-toko/{id}/edit','TokoController@edit');
    Route::post('/jadi-punya-toko','TokoController@store');
    Route::post('/jadi-punya-toko/veriv','TokoController@veriv');
    Route::put('/jadi-punya-toko/{id}','TokoController@update');
    Route::put('/jadi-punya-toko/veriv/{id}','TokoController@cekKode');
});
Route::middleware('auth')->namespace('Frontend\Barang')->group(function () {
    Route::get('/jual-barang','JualBarangController@index');
    Route::get('/jual-barang/{id}','JualBarangController@show');
    Route::post('/jual-barang','JualBarangController@store');
    Route::get('/jual-barang/{id}/edit','JualBarangController@edit');
    Route::put('/jual-barang/{id}','JualBarangController@update');
    Route::delete('/jual-barang/{id}','JualBarangController@delete');
});
// kategori
Route::middleware('auth')->namespace('Frontend\Kategori')->group(function () {
    Route::post('/kategori','KategoriController@subkategori');
    Route::post('/child-kategori','KategoriController@chilKategori');
});

Route::middleware('auth')->namespace('Frontend\Profile')->group(function(){
    Route::resource('myprofile', 'MyprofileController');
});

Route::middleware('auth')->namespace('Frontend\Konsultasi')->group(function(){
    Route::get('konsultasi', 'KonsultasiController@index');
    Route::get('konsultasi/user-aktiv', 'KonsultasiController@userAktiv');
    Route::get('konsultasi/list-chat', 'KonsultasiController@listchat');
    Route::post('konsultasi/add-friend', 'KonsultasiController@addfriend');
    Route::post('konsultasi/start-chat', 'KonsultasiController@startchat');
});

Route::namespace('Frontend\Searching')->group(function(){
    Route::get('/cat-toko/{slug}', 'SearchingController@searchToko');
    Route::get('/aj-toko/render-data', 'SearchingController@renderData');
    Route::post('/aj-toko/filter-data', 'SearchingController@filterData');
});
